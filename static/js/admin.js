var el = document.querySelector('.tabs');
var tabs = M.Tabs.init(el, {});
var elems = document.querySelectorAll('select');
var instances = M.FormSelect.init(elems);
var tooltipped = document.querySelector('.tooltipped');
var tooltips = M.Tooltip.init(tooltipped, {});

$('.modal').modal({onCloseEnd: function (){
        location.reload(true)
    }})

$('.modal-no-reload').modal({onCloseEnd: function (){
        location.reload(true)
    }})

$(document).on('click', '#companies-link', function(e) {
    tabs.select('companies');
});

$(document).on('click', '#users-link', function(e) {
    tabs.select('users');
});

$(document).on('click', '#settings-link', function(e) {
    tabs.select('settings');
});
