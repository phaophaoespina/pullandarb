let $body = $('body');

if (Number(sessionStorage.getItem('sidenav-hidden')))
  $body.addClass('sidenav-hidden');

$(window).resize(function () {
  if ($(document).width() < 993 && $body.hasClass('sidenav-hidden'))
    setTimeout(() => {
      $body.removeClass('sidenav-hidden');
      sessionStorage.setItem('sidenav-hidden', '0');
    }, 200);
})

$('.sidenav-hider').click(function (e) {
  if ($(document).width() < 993)
    return;
  let hidden = $body.hasClass('sidenav-hidden')
  $body.removeAttr('style');
  if (hidden)
    $body.removeClass('sidenav-hidden');
  else
    $body.addClass('sidenav-hidden');
  sessionStorage.setItem('sidenav-hidden', String(Number(!hidden)));
});

$('.sidenav-hider').on('transitionend', function () {
    $('.tabs').tabs('updateTabIndicator');
})
