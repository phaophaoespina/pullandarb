function gettransactions(company_uuid, csrf) {

    let submit_location = "/transactions"
    let inputs = {'company_uuid': company_uuid,
                    'csrf': csrf
                }
    let submission = $.post(submit_location, inputs)

    submission.done(function() {
        console.log("DONE",submission)

        data = submission.responseJSON.transactions
        console.log(data)
        var tableCustomElements = $('#table-custom-elements');
        if (tableCustomElements.length) {
          var table = tableCustomElements.DataTable({
            'data': data,
              "columns": [{ "title": "Date"},
                  { "title": "Exchange"},
                  { "title": "Wallet&nbsp;(Last&nbsp;4)"},
                  { "title": "T/A&nbsp;Type"},
                  { "title": "Currency&nbsp;Type"},
                  { "title": "Quantity"},
                  { "title": "Unit&nbsp;Rate&nbsp;**"},
                  { "title": "Total&nbsp;$USD&nbsp;Equivalent"},
                  { "title": "Transaction&nbsp;Fees"},
                  { "title": "GL&nbsp;Account&nbsp;#"},
                  { "title": "GL&nbsp;Description"},
                  { "title": "Description"}],
            'language': {
              'search': '',
              'searchPlaceholder': 'Enter search term'
            },
            'order': [2, 'asc'],
            'dom': 'ft<"footer-wrapper"l<"paging-info"ip>>',
            //   "scrollX": true,
            // 'scrollY': '400px',
            'scrollCollapse': true,
            'pagingType': 'full',
            'responsive': true,

          });
          $("th.sorting, th.sorting_asc, th.sorting_desc").css({
            'background-image': 'none',
            'cursor': 'default'
          });
          $('#table-custom-elements_wrapper').on('change', 'input[type=checkbox]', function(e) {
            var parentTR = $(this).parentsUntil('table').closest('tr');
            parentTR.toggleClass('selected', this.checked);
          });

          // Handle click on "Select all" control
          $('#table-custom-elements_wrapper').find('.select-all').on('click', function(){
            // Check/uncheck all checkboxes in the table
            var rows = table.rows({ 'search': 'applied' }).nodes();
            $('input[type="checkbox"]', rows)
              .prop('checked', this.checked)
            $(rows).toggleClass('selected', this.checked);
          });
        }
    });
    submission.fail(function() {
        console.log("Fail",submission)
    });

}
