function submit_to_modal(inputs, location, method='post') {
    let submission
    switch (method){

        case "post":
            submission = $.post(location, inputs)
            break
        case "delete":
            submission = $.delete(location, inputs)
            break
        case "put":
            submission = $.put(location, inputs)
            break
        case "patch":
            submission = $.patch(location, inputs)
            break
    }

    submission.done(function() {
        $('#addcompanymodal').modal('close');
        $('#whitelisttokenmodal').modal('close');

        if (submission.responseJSON.url) {
            $('#result_modal_text').html(`<a href='${submission.responseJSON.url}'>${submission.responseJSON.message}</a>`);
            $("#closemodal").attr("href", "#reports")
        } else if(submission.responseJSON.hasOwnProperty('target')){
            $('#result_modal_text').text(submission.responseJSON.message);
            $("#closemodal").attr("href", submission.responseJSON['target'])

        }
        else {
            $('#result_modal').modal('open');
            $('#result_modal_text').text(submission.responseJSON.message);

        }
    });
    submission.fail(function() {
        $('#result_modal').modal('open');
        $('#result_modal_text').text('failure, see console');
    });
}