pysqlite3
behave~=1.2.6
selenium~=3.141.0
coverage==5.3
pytest==6.1.2
flake8>=3.8
flake8-html==0.4.1
anybadge==1.7.0
