import importlib
import sys
from init_celery import celapp
from lib.email import SES
from lib.axieinfinity import Axie
import logging


@celapp.task()
def send_email_task(address, subject, message):
    try:
        # should validate email here
        SES().send_email(address, subject, message)
        pass
    except Exception as exc:
        print(f'ERROR: {exc}')
        return

@celapp.task
def get_latest_address_data():
    axie_infinity = Axie()

    try:
        print("Getting latest address data... ")
        logging.info("Getting latest address data... ")
        # axie_infinity.load_addresses()
        axie_infinity.get_address_data()
    except Exception as exc:
        print(f'ERROR: {exc}')

@celapp.task
def test_celery_queue():

    try:
        print("Getting latest address data... ")
        logging.info("Getting latest address data... ")
    except Exception as exc:
        print(f'ERROR: {exc}')


IN_CELERY_WORKER_PROCESS = sys.argv and sys.argv[0].endswith('celery') and 'worker' in sys.argv
if IN_CELERY_WORKER_PROCESS:
    # for exchange tasks' database dependency
    # circular import error on functional test if imported in main process
    import app

