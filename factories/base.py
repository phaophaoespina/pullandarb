import logging
import transaction
from lib.helpers import settings
from lib import totp
from functools import wraps
from sqlalchemy.exc import IntegrityError, ResourceClosedError, DisconnectionError, OperationalError
from models import (
    University,
    DBSESSION,
    LOGGER,
    User,
    UserUniversityMap,
    UserSettings,
    AxieDataResponse,
    AdminUserMap
)


def safe_db_access(f):
    """
    Wraps the function in a single transaction detaches the returned models.
    f can be any method which returns a sequence of models
    models returned can be used without fear of unintended sql updates
    """

    @wraps(f)
    def wrapper(*args, **kwargs):
        close_needed = False
        if args[0].session is None:
            close_needed = True
            args[0].session = DBSESSION()
        try:
            models = f(*args, **kwargs)
            [args[0].session.expunge(m) for m in models]
            if close_needed:
                DBSESSION.remove()
                args[0].session = None
        except OperationalError:
            transaction.abort()
            if args[-1] == "operationalerror":
                raise
            raise DisconnectionError()

        return models

    return wrapper


def SessionManageDecorator(func):
    def _decorator(self, *args, **kwargs):
        close_needed = False
        if self.session is None:
            close_needed = True
            self.session = DBSESSION()
        result = func(self, *args, **kwargs)
        if close_needed:
            DBSESSION.remove()
            self.session = None
        return result

    return _decorator


def now():
    import datetime
    return datetime.datetime.now()


class BaseFactory(object):
    """
        Base factory providing basic CRUD operations.
    """
    session = None

    def __init__(self, type_, session=None):
        self.type = type_
        self.session = session

    def __check_for_attribute(self, attr, obj_):
        if attr not in obj_.__dict__:
            return False
        return True

    def __update_date_stamp(self, obj_):
        if self.__check_for_attribute("date_updated", obj_):
            obj_.date_updated = now()
        return obj_

    @SessionManageDecorator
    def get_count(self, filters=[]):
        from sqlalchemy import func
        self.session.expire_on_commit = False
        query = self.session.query(func.count(self.type.uuid))
        if len(filters):
            query = query.filter(*filters)
        return query.scalar()

    @SessionManageDecorator
    def get_query(self, limit=None, sort=None, reversed=False, filters=[]):
        """ Get a query of my type """
        self.session.expire_on_commit = False
        query = self.session.query(self.type)
        if len(filters):
            query = query.filter(*filters)
        if sort is not None:
            query = query.order_by(sort)
        elif reversed:
            query = query.order_by(self.type.uuid.desc())
        if limit is not None:
            query = query.limit(limit)
        return query

    def mutable_get(self, limit=None, sort=None, reversed=False, filters=[]):
        return self.get_query(limit=limit, sort=sort, reversed=reversed,
                              filters=filters).all()

    @safe_db_access
    def get(self, *args, **kwargs):
        return self.mutable_get(*args, **kwargs)

    @safe_db_access
    def filter_by(self, **kwargs):
        """ Retrieve a query filtered by the kwargs """
        query = self.get_query()
        return query.filter_by(**kwargs)

    @SessionManageDecorator
    def get_by_attribute(self, attribute, value):
        """ Get the item with the given name """
        args = {attribute: value}
        try:
            return self.filter_by(**args).scalar()
        except Exception as e:
            logging.warning("Attribute error: {}".format(e))
            return None

    @SessionManageDecorator
    def get_all_by_attribute(self, attribute, value):
        """ Get the item with the given name """
        args = {attribute: value}
        return self.filter_by(**args).all()

    @SessionManageDecorator
    def get_by_id(self, id_):
        """ Retrieve the object with the given uuid. """
        try:
            return self.filter_by(uuid=id_).scalar()
        except Exception as e:
            logging.warning("Attribute error: {}".format(e))
            return None

    @SessionManageDecorator
    def delete(self, id_):
        """ Delete the object with the given id. """
        self.session.expire_on_commit = False
        LOGGER.debug("delete received id %s" % id_)
        object_ = self.get_by_id(id_)
        if object_ is not None:
            self.session.delete(object_)
            self.session.flush()
            transaction.commit()
        else:
            LOGGER.debug("tried deleting a nonexistent item")

    def delete_all(self, obj_list):
        """
        For a list of objects with ids, delete them all
        :param obj_list: The list of objects to be deleted
        """
        to_delete = list()
        for obj in obj_list:
            to_delete.append(obj.uuid)
        for each in to_delete:
            self.delete(each)

    @SessionManageDecorator
    def add(self, object_):
        """ Add the given object to the system. """
        self.session.expire_on_commit = False
        if object_ is None:
            raise Exception('None type object received')
        try:
            self.session.add(object_)
            self.session.flush()
            obj_id = object_.uuid
            transaction.commit()
        except IntegrityError:
            self.session.rollback()
            transaction.abort()
            raise
        except ResourceClosedError:
            transaction.abort()
            raise
        return self.get_by_id(obj_id)

    @SessionManageDecorator
    def merge(self, object_):
        """ Update object when obj is not currently in session scope """
        self.session.expire_on_commit = False
        if object_ is None:
            raise Exception('None type object received')
        try:
            assert object_ not in self.session
            obj = self.session.merge(object_)
            obj_id = obj.uuid
            self.session.flush()
            transaction.commit()
        except IntegrityError:
            self.session.rollback()
            transaction.abort()
            raise
        except ResourceClosedError:
            transaction.abort()
            raise
        return self.get_by_id(obj_id)

    @SessionManageDecorator
    def update(self, object_):
        """ Update object """
        self.session.expire_on_commit = False
        if object_ is None:
            raise Exception('None type object received')
        object_ = self.__update_date_stamp(object_)
        if not self.in_session(object_):
            return self.merge(object_)
        try:
            self.get_by_id(object_.uuid)
            obj = object_
            obj_id = obj.uuid
            self.session.flush()
            transaction.commit()
        except IntegrityError:
            self.session.rollback()
            transaction.abort()
            raise
        except ResourceClosedError:
            transaction.abort()
            raise
        return self.get_by_id(obj_id)

    @SessionManageDecorator
    def in_session(self, object_):
        if object_ not in self.session:
            return False
        return True

    @SessionManageDecorator
    def flush(self):
        self.session.flush()
        transaction.commit()

    @SessionManageDecorator
    def refresh(self, obj):
        self.session.refresh(obj)

    @SessionManageDecorator
    def raw_query(self, command):

        return self.session.execute(command)


class UUID(object):

    def unique_uuid(self):
        import uuid
        unique = False
        while not unique:
            temp_uuid = str(uuid.uuid4())
            obj = self.get_by_attribute("uuid", temp_uuid)
            if obj is None:
                return temp_uuid


class AxieDataResponseFactory(UUID, BaseFactory):

    def __init__(self):
        BaseFactory.__init__(self, AxieDataResponse)

    def create(self, user_uuid, ign, updated_on, total_slp, in_game_slp, ronin_slp, win_rate, scholar_payout,
               mmr, rank, total_matches, last_claim_amount, last_claim_timestamp):

        axiedata = self.type(user_uuid, ign, updated_on, total_slp, in_game_slp, ronin_slp, win_rate,
                             mmr, rank, total_matches, last_claim_amount, last_claim_timestamp, scholar_payout,)

        return self.add(axiedata)


class UserFactory(UUID, BaseFactory):

    def __init__(self):
        BaseFactory.__init__(self, User)

    def check_user_credentials(self, username, password):
        user = self.get_by_attribute("username", username)
        if user is not None and user.validate(password):
            return user
        return None

    def create(self, username, authority="scholar", password=None, status="Inactive", secret_key=None,
               auth_status="Disabled", team_aquisition_cost=None, team_current_valuation=None, qr_data=None,
):
        user = self.type(username, authority, password, status, secret_key, auth_status,
                         team_aquisition_cost, team_current_valuation, qr_data)
        return self.add(user)

    def get_user_list_by_authority(self, authority):
        filters = [
            self.type.authority == authority
        ]
        users = self.get(filters=filters, limit=100)
        user_list = list()
        for user in users:
            user_list.append({"name": user.username, "role": user.authority, "uuid": user.uuid, "status": user.status,
                              "app_login": user.app_login, "app_password": user.app_password,
                              "ronin_address": user.ronin_address, "scholar_share": user.scholar_share})
        return sorted(user_list, key=lambda _: _['role'])

    def create_secret_key(self):
        return totp.random_base32()

    def generate_otp(self, secret):
        return totp.generate_otp(secret)

    def verify_otp(self, otp, secret):
        return totp.verify(otp, secret)


class AdminUserMapFactory(UUID, BaseFactory):

    def __init__(self):
        BaseFactory.__init__(self, AdminUserMap)

    def create(self, admin_uuid, user_uuid):
        mapping = self.type(admin_uuid, user_uuid)
        return self.add(mapping)

    def retrieve_mapping(self, admin, user):
        filters = [
            self.type.admin_uuid == admin,
            self.type.user_uuid == user
        ]
        results = self.get(filters=filters)
        if len(results) == 0:
            return None
        elif len(results) == 1:
            return results[0]
        else:
            return results

    def retrieve_all_user_mappings(self, user):
        filters = [
            self.type.user_uuid == user
        ]
        results = self.get(filters=filters)
        if len(results) == 0:
            results = None
        return results


class UserUniversityMapFactory(UUID, BaseFactory):

    def __init__(self):
        BaseFactory.__init__(self, UserUniversityMap)
        BaseFactory.__init__(self, UserUniversityMap)

    def create(self, user_uuid, university_uuid):
        mapping = self.type(user_uuid, university_uuid)
        return self.add(mapping)

    def retrieve_mapping(self, university, user):
        filters = [
            self.type.user_uuid == user,
            self.type.university_uuid == university
        ]
        results = self.get(filters=filters)
        if len(results) == 0:
            return None
        elif len(results) == 1:
            return results[0]
        else:
            return results

    def retrieve_all_user_mappings(self, user):
        filters = [
            self.type.user_uuid == user
        ]
        results = self.get(filters=filters)
        if len(results) == 0:
            results = None
        return results


class UniversityFactory(UUID, BaseFactory):

    def __init__(self):
        BaseFactory.__init__(self, University)

    def create(self, name, user):
        university = self.type(name, user)
        return self.add(university)

    def get_universities_by_creator(self, user_uuid):
        filters = [
            self.type.created_by == user_uuid
        ]
        results = self.get(filters=filters, limit=100)
        if len(results) == 0:
            results = None
        return results


class UserSettingsFactory(UUID, BaseFactory):

    def __init__(self):
        BaseFactory.__init__(self, UserSettings)

    def create(self, created_by, university_uuid, settings):
        setting = self.type(created_by, university_uuid, settings)
        return self.add(setting)

    def get_settings(self, university_uuid, user_uuid):
        filters = [
            self.type.university_uuid == university_uuid,
            self.type.created_by == user_uuid
        ]
        return self.get(filters=filters)

    def retrieve_json(self, university_uuid, user_uuid):
        filters = [
            self.type.university_uuid == university_uuid,
            self.type.created_by == user_uuid
        ]
        results = self.get(filters=filters)
        data = list()
        if results:
            for res in results:
                data.append(res.settings)
            return eval(data[0])
        else:
            return eval(settings['default_table_settings'])
