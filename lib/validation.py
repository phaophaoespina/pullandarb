from formencode import Schema, Invalid, FancyValidator
from formencode.validators import Email, String
from datetime import datetime
from backports.datetime_fromisoformat import MonkeyPatch
MonkeyPatch.patch_fromisoformat()

class ValidUUID(FancyValidator):
    def _to_python(self, value, state):
        from uuid import UUID
        try:
            UUID(value)
        except ValueError:
            raise Invalid("Invalid format", value, state)
        return value


class Date8601(FancyValidator):
    def _to_python(self, value, state):
        try:
            datetime.fromisoformat(value)
        except ValueError:
            raise Invalid("Invalid format", value, state)
        return value


class InList(FancyValidator):

    def _to_python(self, value, state):
        if value.lower() in self.approved:
            return value
        raise Invalid("Not one of the available options", value, state)


class ReportType(InList):

    def __init__(self):
        self.approved = ["fills", "accounts"]


class CoinbaseReportFormat(InList):

    def __init__(self):
        self.approved = ["csv", "pdf"]


class CoinbaseReportType(InList):

    def __init__(self):
        self.approved = ["transactions", "accounts"]


class CompanyExchange(Schema):
    company_uuid = ValidUUID()
    map_uuid = ValidUUID()
    # Todo can make fancy validator to check UUID belongs to correlating exchange
    exchange_uuid = ValidUUID()
    exchange_name = String(if_missing=None)



class CoinbaseValidator(CompanyExchange):
    report_type = CoinbaseReportType()
    start_date = Date8601()
    end_date = Date8601()


class CoinbaseProValidator(CompanyExchange):
    report_type = ReportType()
    start_date = Date8601()
    end_date = Date8601()
    report_format = CoinbaseReportFormat()
    email = Email(if_missing=None)



class CoinbasePrimeValidator(CoinbaseProValidator):
    pass


class EthereumValidator(CompanyExchange):
    pass

class AxieValidator(CompanyExchange):
    pass

class KucoinValidator(CompanyExchange):
    report_type = ReportType()
    start_date = Date8601()
    end_date = Date8601()


class BinanceUSValidator(CompanyExchange):
    report_type = ReportType()
    start_date = Date8601()
    end_date = Date8601()


class BinanceGeneralValidator(BinanceUSValidator):
    pass


class BittrexValidator(CompanyExchange):
    report_type = ReportType()
    start_date = Date8601()
    end_date = Date8601()


class FTXValidator(CompanyExchange):
    report_type = ReportType()
    start_date = Date8601()
    end_date = Date8601()


class HitBTCValidator(CompanyExchange):
    report_type = ReportType()
    start_date = Date8601()
    end_date = Date8601()


class AnchorageValidator(CompanyExchange):
    report_type = ReportType()
    start_date = Date8601()
    end_date = Date8601()