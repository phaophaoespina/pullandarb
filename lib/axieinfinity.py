import time

import cryptoaddress
import requests
import xlsxwriter
import logging
import itertools
from hashlib import sha1
from datetime import datetime
from xlsxwriter.exceptions import DuplicateWorksheetName
import urllib3
from factories.base import AxieDataResponseFactory, UserFactory
from lib.helpers import add_row, settings
from datetime import datetime, timedelta


class Axie():
    balances = dict()
    consol_token_row = 0
    consol_eth_row = 0
    company_name = ''
    axie_data_response_factory = AxieDataResponseFactory()
    addresses = []
    user_factory = UserFactory()

    @staticmethod
    def keys():
        return [{"name": "api_addresses", "placeholder": "Addresses"},
                {"name": "address_eth", "placeholder": "Eth Address"}
                ]

    @staticmethod
    def report_options():
        return {}

    def __init__(self, api_addresses=None, addresses=None):
        self.load_addresses(addresses)
        super().__init__()

    def load_addresses(self, addresses, ):
        self.addresses = []
        if addresses:
            self.addresses.extend(addresses)

    def get_daily_slp(self, data):
        # returns list of daily SLP, with date and time
        daily_slps_per_address = list()
        for address in data:
            daily_slp = list()
            address_key = next(iter(address))
            timeseries_data = address[address_key]

            print(address_key)

            dates = [datetime.strptime(i.updated_on, '%Y-%m-%d %H:%M') for i in timeseries_data]
            dates = list(set(dates))

            days = [list(g) for k, g in itertools.groupby(dates, key=lambda d: d.date())]

            for day in days:
                newest = max(day)
                find_count = [item for item in timeseries_data if item.updated_on == str(newest).replace(":00", "")]
                daily_dict = {
                    'slp': find_count[0].total_slp,
                    'updated_on': newest
                }
                daily_slp.append(daily_dict)

            dict = {
                address_key : daily_slp
            }
            daily_slps_per_address.append(dict)

        new_list = list()
        for data in daily_slps_per_address:
            daily_slps = list()
            address_key = next(iter(data))
            timeseries_data = data[address_key]
            for x, y in zip(timeseries_data, timeseries_data[1:]):
                res = int(x['slp']) - int(y['slp'])
                if res > 0:
                    new_data = {
                        'slp': res,
                        'updated_on': x['updated_on']
                    }
                    daily_slps.append(new_data)

            user = self.user_factory.get_by_id(address_key)
            dict_ = {
                'user' : str(user.username),
                'daily_slps': daily_slps }
            new_list.append(dict_)

        return new_list

    def get_slp_latest_price(self):

        try:

            slp_php_path = "https://api.coingecko.com/api/v3/simple/price?ids=smooth-love-potion&vs_currencies=php"
            res = requests.get(slp_php_path)
            data = res.json()
            slp_php = data["smooth-love-potion"]["php"]

            slp_php_path = "https://api.coingecko.com/api/v3/simple/price?ids=smooth-love-potion&vs_currencies=usd"
            res = requests.get(slp_php_path)
            data = res.json()
            slp_usd = data["smooth-love-potion"]["usd"]
        except Exception:
            slp_php = 0
            slp_usd = 0

        print("SLP Current Price: {}".format(slp_usd))
        return {"php": slp_php, "usd": slp_usd}

    def get_address_data(self):
        lunicia_rover = "https://api.lunaciarover.com/stats/{}"
        for address in self.addresses:
            api_url = lunicia_rover.format(address['ronin'])
            res = requests.get(api_url)
            if res.status_code == 400:
                return list()

            if res.status_code == 500:
                return list()

            response_json = res.json()
            try:
                last_claim_time = datetime.fromtimestamp(response_json['last_claim_timestamp']).isoformat()
                updated_time = datetime.fromtimestamp(response_json['updated_on']).isoformat()

                ronin_address = response_json['ronin_address']
                ronin_address = ronin_address.replace("0x", "ronin:")
                marketplace_url = "https://marketplace.axieinfinity.com/profile/{}/axie".format(ronin_address)

                response_json['marketplace_url'] = marketplace_url
                response_json['last_claim_timestamp'] = datetime.strptime(last_claim_time, "%Y-%m-%dT%H:%M:%S").strftime("%Y-%m-%d %H:%M")
                response_json['updated_on'] = datetime.strptime(updated_time, "%Y-%m-%dT%H:%M:%S").strftime("%Y-%m-%d %H:%M")

                user_setting = self.user_factory.get_by_id(address['user_uuid'])
                percentage_to_dec = int(user_setting.scholar_share) / 100
                scholar_payout = float(response_json['total_slp']) * float(percentage_to_dec)

                axie_response_exists = self.axie_data_response_factory.get_by_attribute\
                    ('user_uuid', address['user_uuid'])

                if axie_response_exists:
                    self.axie_data_response_factory.update(
                        user_uuid=address['user_uuid'],
                        ign=response_json['ign'],
                        updated_on=response_json['updated_on'],
                        total_slp=response_json['total_slp'],
                        in_game_slp=response_json['in_game_slp'],
                        ronin_slp=response_json['ronin_slp'],
                        win_rate=response_json['win_rate'],
                        scholar_payout=scholar_payout,
                        mmr=response_json['mmr'],
                        rank=response_json['rank'],
                        total_matches=response_json['total_matches'],
                        last_claim_amount=response_json['last_claim_amount'],
                        last_claim_timestamp=response_json['last_claim_timestamp'],
                    )
                else:
                    self.axie_data_response_factory.create(
                        user_uuid=address['user_uuid'],
                        ign=response_json['ign'],
                        updated_on=response_json['updated_on'],
                        total_slp=response_json['total_slp'],
                        in_game_slp=response_json['in_game_slp'],
                        ronin_slp=response_json['ronin_slp'],
                        win_rate=response_json['win_rate'],
                        scholar_payout=scholar_payout,
                        mmr=response_json['mmr'],
                        rank=response_json['rank'],
                        total_matches=response_json['total_matches'],
                        last_claim_amount=response_json['last_claim_amount'],
                        last_claim_timestamp=response_json['last_claim_timestamp'],
                    )
            except KeyError as e:
                print(e)
                pass
            except requests.exceptions.ConnectionError:
                pass
            except Exception as e:
                pass

    def get_latest_addresses_data(self):
        data_list = list()
        for address in self.addresses:
            data = self.axie_data_response_factory.get_all_by_attribute("user_uuid", address['user_uuid'])
            if len(data) == 1:
                data_list.append(data[0])
            elif len(data) > 1:
                data_list.append(data[-1])
        return data_list

    def get_all_adddress_data(self):
        data_list = list()
        print(self.addresses)
        for address in self.addresses:
            data = self.axie_data_response_factory.get_all_by_attribute("user_uuid", address['user_uuid'])
            data_dict = {
                str(address['user_uuid']): data
            }
            data_list.append(data_dict)
        return data_list

    def profit_loss_calculation(self, team_aqc, team_val, slp_cash):
        if not team_val:
            team_val = 0
        if not team_aqc:
            team_aqc = 0

        team_profit = float(team_val) - float(team_aqc)
        all_profit = slp_cash + team_profit
        return all_profit

    def get_latest_profit_loss_data(self, addresses, slp_price):
        profit_data_list = list()
        for address in addresses:
            user_setting = self.user_factory.get_by_id(address.user_uuid)
            manager_share = 100 - int(user_setting.scholar_share)
            percentage_to_dec = int(manager_share) / 100

            manager_payout = float(address.total_slp) * float(percentage_to_dec)

            data = dict()
            data['slp_cash_to_date'] = round(manager_payout * slp_price, 2)
            data['team_aquisition_cost'] = user_setting.team_aquisition_cost
            data['team_current_valuation'] = user_setting.team_current_valuation
            data['profit_loss_calculation'] = self.profit_loss_calculation(
                user_setting.team_aquisition_cost,
                user_setting.team_current_valuation,
                manager_payout
            )
            data['ign'] = address.ign
            data['updated_on'] = address.updated_on
            profit_data_list.append(data)

        return profit_data_list

    def __to_datetime(self, ts):
        timestamp = datetime.fromtimestamp(ts)
        return timestamp.strftime('%Y-%m-%d %H:%M:%S')

    def __get_transactions(self):
        print("getting_transactions")
        ethplorer = "https://api.ethplorer.io/getAddressHistory/{}?apiKey={}&limit=100"
        api_key = settings['ethplorer.apikey']
        transactions = dict()
        for address in self.addresses:
            cryptoaddress.EthereumAddress(address)
            res = requests.get(ethplorer.format(address, api_key))
            transactions[address] = res.json()
        return transactions
