import hashlib
import base64
import hmac
import time
import random
import calendar
import datetime


def random_base32() -> str:
    chars = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ234567')
    return ''.join(
        random.choice(chars)
        for _ in range(32)
    )


def generate_otp(secret) -> str:
    _time = time_code()
    hasher = hmac.new(byte_secret(secret), int_to_bytestring(_time), hashlib.sha1)
    hmac_hash = bytearray(hasher.digest())
    offset = hmac_hash[-1] & 0xf
    code = ((hmac_hash[offset] & 0x7f) << 24 |
            (hmac_hash[offset + 1] & 0xff) << 16 |
            (hmac_hash[offset + 2] & 0xff) << 8 |
            (hmac_hash[offset + 3] & 0xff))
    str_code = str(code % 10 ** 6)
    while len(str_code) < 6:
        str_code = '0' + str_code

    return str_code


def byte_secret(secret):
    missing_padding = len(secret) % 8
    if missing_padding != 0:
        secret += '=' * (8 - missing_padding)
    return base64.b32decode(secret, casefold=True)


def int_to_bytestring(input):
    result = bytearray()
    while input != 0:
        result.append(input & 0xFF)
        input >>= 8
    return bytes(bytearray(reversed(result)).rjust(8, b'\0'))


def time_code():
    if datetime.datetime.now().tzinfo:
        return int(calendar.timegm(datetime.datetime.now().utctimetuple()) / 30)
    else:
        return int(time.mktime(datetime.datetime.now().timetuple()) / 30)


def verify(otp, secret):
    totp = generate_otp(secret)
    if otp == totp:
        return True
    else:
        return False
