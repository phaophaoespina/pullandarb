import configparser
import logging
import os
import pickle
import re

from base64 import b64encode, b64decode
from celery import Celery
from celery.schedules import crontab
from lib.noncemanager import SignedNonceManager


filename = os.getenv("configfile")
if filename is None:
    filename = "config.ini"
config = configparser.ConfigParser()
config.read(filename)
settings = config['info']

celery_config = config['celery']
sentry_config = config['sentry']


def date_format(date_to_convert):
    """ Change a python datetime to a YYYY-MM-DD HH:MM:SS """
    import datetime
    if isinstance(date_to_convert, datetime.date):
        return date_to_convert.strftime("%Y-%m-%d %H:%M:%S")
    elif isinstance(date_to_convert, str) or isinstance(date_to_convert, bytes):
        return datetime.datetime.strptime(date_to_convert[:19], "%Y-%m-%d %H:%M:%S")
    return None


def transaction_date_format(date_to_convert):
    """ Change a python datetime to a MM-DD-YY HH:MM:SS """
    from datetime import datetime
    if isinstance(date_to_convert, str) or isinstance(date_to_convert, bytes):
        return datetime.strptime(date_to_convert[:10], "%Y-%m-%d").strftime('%m/%d/%y')
    return None


def gen_hash_password(password, length=32):
    """Generates a hashed password"""
    import random
    letters = 'abcdefghijklmnopqrstuvwxyz0123456789'
    passw = ''
    random.seed(password)
    for x in range(length):
        passw += letters[random.randint(0, len(letters)-1)]
    return passw


def generate_password(length=32):
    """Generates a password"""
    import random
    import string
    import datetime
    random.seed(datetime.datetime.now())
    vc = string.ascii_letters + string.digits
    h1 = ''.join([random.choice(vc) for i in range(32)])
    return gen_hash_password(h1, length)


class SessionManager(object):

    session = None

    def __init__(self, session):
        self.session = session

    def __getitem__(self, item):
        if self.session.get(item) is None:
            return None
        try:
            return redis_decode(self.session[item])
        except Exception as e:
            logging.info("Exception found decoding: {}".format(e))
            return self.session[item]

    def __setitem__(self, key, value):
        try:
            self.session[key] = value
        except Exception as e:
            logging.info("Exception found encoding: {}".format(e))
            self.session[key] = redis_encode(value)

    def __contains__(self, item):
        return item in self.session

    def get(self, item):
        return self.__getitem__(item)

    def __delitem__(self, key):
        del self.session[key]


def redis_encode(data):
    if type(data) in [SignedNonceManager, dict]:
        return b64encode(pickle.dumps(data))
    raise


def redis_decode(data):
    decoded = b64decode(data.encode())
    return pickle.loads(decoded)


def add_row(worksheet, row, data):
    count = 0
    for each in data:
        worksheet.write(row, count, each)
        count += 1


def add_rows(worksheet, header, data, start_row=0):
    row_number = start_row
    add_row(worksheet, row_number, header)
    for row in data:
        row_number += 1
        data = list()
        for item in header:
            try:
                d = dict_ns_get(row, item)
                if isinstance(d, list):
                    d = '\n'.join(d)
                data.append(d)
            except KeyError:
                data.append("")
        add_row(worksheet, row_number, data)


def dict_ns_get(_dict: dict, ns_path: str):
    """Get `_dict` data via `ns_path` (e.g _dict={'accounts': [{'id': 1}]}, ns_path='accounts[0].id')."""
    for key in ns_path.split('.'):
        _key_idx = re.findall(r'(.+)\[(\d+)\]', key)
        if _key_idx:
            key = _key_idx[0][0]
        _dict = _dict[key]
        if _key_idx:
            index = int(_key_idx[0][1])
            _dict = _dict[index]
    return _dict


def make_celery(app_name=__name__):
    class CeleryConfig:
        RABBITMQ_USER = celery_config['RABBITMQ_USER']
        RABBITMQ_PASSWORD = celery_config['RABBITMQ_PASSWORD']
        RABBITMQ_HOST = celery_config['RABBITMQ_HOST']
        RABBITMQ_PORT = celery_config['RABBITMQ_PORT']
        RABBITMQ_VHOST = celery_config['RABBITMQ_VHOST']
        broker_pool_limit = None
        broker_heartbeat = 0
        broker_url = f'amqp://{RABBITMQ_USER}:{RABBITMQ_PASSWORD}@{RABBITMQ_HOST}:{RABBITMQ_PORT}/{RABBITMQ_VHOST}'
        result_backend = celery_config['CELERY_RESULT_BACKEND']

        beat_schedule = {
            'run-fetch_exchange_historicaldata': {
                # 'task': 'tasks.get_latest_address_data',
                'task': 'tasks.test_celery_queue',
                'schedule': crontab(),
                # 'schedule': crontab(minute=0, hour=8),  # run tasks every 8:00 AM UTC
            },
        }

    celery_app = Celery(app_name,)
    celery_app.config_from_object(CeleryConfig)

    return celery_app


from bottle_session import Session, SessionPlugin
from bottle import request
import redis
import inspect


class KludgeSessionPlugin(SessionPlugin):

    def apply(self,callback,context):
        conf = context.config.get('session') or {}
        # args = context.get_callback_args()

        # Update for python 3.4+ but fallback to python 2.7+ compatible
        # argument inspection. This is similar to bottle-sqlalchemy code and
        # bottle-redis.
        try:
            args = inspect.signature(context.callback).parameters
        except AttributeError:
            args = inspect.getargspec(context.callback)[0]

        if self.keyword not in args:
            return callback

        def wrapper(*args,**kwargs):
            r = redis.Redis(connection_pool=self.connection_pool)
            kwargs[self.keyword] = KludgeSession(r,self.cookie_name,self.cookie_lifetime, self.cookie_secure, self.cookie_httponly)
            rv = callback(*args,**kwargs)
            return rv
        return wrapper


class KludgeSession(Session):

    def get_cookie(self):
        uid_cookie = request.get_cookie(self.cookie_name)
        if uid_cookie is None:
            uid_cookie = request.get_cookie(self.cookie_name.replace(".", ""))
        return uid_cookie
