import boto3
from botocore.exceptions import ClientError
from lib.helpers import settings
import smtplib
import ssl


class SES(object):

    def __init__(self):
        access_key = settings['aws.access_key']
        secret_key = settings['aws.secret_key']
        region = settings['aws.region']
        self.sender = settings['aws.ses_sender']
        self.client = boto3.client('ses',
                                   region_name=region,
                                   aws_access_key_id=access_key,
                                   aws_secret_access_key=secret_key)

    def send_email(self, to, subject, body):
        # Try to send the email.
        CHARSET = "UTF-8"
        try:
            # Provide the contents of the email.
            response = self.client.send_email(
                Destination={
                    'ToAddresses': [
                        to,
                    ],
                },
                Message={
                    'Body': {
                        'Text': {
                            'Charset': CHARSET,
                            'Data': body,
                        },
                    },
                    'Subject': {
                        'Charset': CHARSET,
                        'Data': subject,
                    },
                },
                Source=self.sender,
            )
        # Display an error if something goes wrong.
        except ClientError as e:
            print(e.response['Error']['Message'])
        else:
            print("Email sent! Message ID:"),
            print(response['MessageId'])


class SMTP(object):

    def __init__(self, host=None, username=None, password=None, port=None):
        if host is None:
            host = settings['smtp.host']
        if username is None:
            username = settings['smtp.username']
        if password is None:
            password = settings['smtp.password']
        if port is None:
            port = int(settings['smtp.port'])

        self.context = ssl.create_default_context()
        self.host = host
        self.username = username
        self.password = password
        self.port = port

    def send_email(self, to, subject, body):

        if self.port == 587:
            with smtplib.SMTP(self.host, self.port) as client:
                client.ehlo()
                client.starttls(context=self.context)
                client.ehlo()
                client.login(self.username, self.password)
                body = "Subject: " + subject + "\n\n" + body
                client.sendmail(self.username, to, body)
        elif self.port == 465:
            with smtplib.SMTP_SSL(self.host, self.port, self.context) as client:
                client.ehlo()
                client.login(self.username, self.password)
                body = "Subject: " + subject + "\n\n" + body
                client.sendmail(self.username, to, body)
