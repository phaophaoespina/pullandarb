# syntax=docker/dockerfile:1

FROM python:3.8.11-buster

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN pip3 install bottle

COPY . .

CMD [ "python3", "app.py", "--host=0.0.0.0"]