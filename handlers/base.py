from bottle import response, request
from lib.noncemanager import SignedNonceManager
from lib.helpers import SessionManager, settings
import json
import sys
import logging


def JSONResponse(func):
    def _decorator(self, *args, **kwargs):
        response.headers['Content-Type'] = 'application/json'
        f = func(self, *args, **kwargs)
        return json.dumps(f)

    return _decorator


class BaseHandler(object):

    request = None
    session = None
    nonce_manager = None

    def __init__(self, session, action):
        self.request = request
        self.session = SessionManager(session)
        session_value = self.session.session.session_hash.split(":")[1].replace("-", "")
        response.set_cookie("iskosession", session_value, path='/', httponly=True)
        self._set_browser_and_os()
        if "nonce_manager" not in self.session:
            self.session['nonce_manager'] = SignedNonceManager(timeout=7200)
        val = self.session['nonce_manager']
        self.nonce_manager = val
        # Routing
        try:
            if self.request.method == 'GET':
                if hasattr(self, action):
                    self.response = getattr(self, action)()
            elif self.request.method == 'POST':
                if self._validate(action):
                    self.response = getattr(self, action + "_post")()
                else:
                    self.response = self.error_page()
            elif self.request.method == 'DELETE':
                if self._validate(action):
                    self.response = getattr(self, action + "_delete")()
                else:
                    self.response = self.error_page()
            elif self.request.method == 'PATCH':
                if self._validate(action):
                    self.response = getattr(self, action + "_patch")()
                else:
                    self.response = self.error_page()
            elif self.request.method == 'PUT':
                if self._validate(action):
                    self.response = getattr(self, action + "_put")()
                else:
                    self.response = self.error_page()
        except Exception as error:
            logging.warning("Exception at: {}".format(error))
            e = sys.exc_info()[0]
            logging.info("sys.exc_info: {}".format(e))
            if settings.get('test') == 'true':
                raise
            self.response = self.error_page()

    def _set_browser_and_os(self):
        user_agent = self.request.headers.get("User-Agent")
        if user_agent is None:
            self.request.browser = 'unknown'
            self.request.os = 'unknown'
        else:
            # Browser
            if 'Firefox' in user_agent and 'Seamonkey' not in user_agent:
                self.request.browser = 'firefox'
            elif 'Chrome' in user_agent and 'Chromium' not in user_agent:
                self.request.browser = 'chrome'
            elif 'Safari' in user_agent and 'Chrome' not in user_agent and 'Chromium' not in user_agent:
                self.request.browser = 'safari'
            elif 'MSIE' in user_agent:
                self.request.browser = 'ie'
            elif 'Opera' in user_agent:
                self.request.browser = 'opera'
            elif 'Seamonkey' in user_agent:
                self.request.browser = 'seamonkey'
            elif 'Chromium' in user_agent:
                self.request.browser = 'chromium'
            else:
                self.request.browser = 'unknown'
            # Operating System
            if 'Macintosh' in user_agent:
                self.request.os = 'mac'
            elif 'Windows' in user_agent:
                self.request.os = 'windows'
            elif 'Linux' in user_agent:
                self.request.os = 'linux'
            else:
                self.request.os = 'unknown'

    def _set_csrf(self, form_name):
        from lib.helpers import generate_password
        import datetime
        if "csrf" + form_name in self.session:
            csrf = self.session['csrf' + form_name]
        else:
            self.session['csrf' + form_name] = csrf = generate_password()

        if self.session.get('nonces') is None:
            self.session['nonces'] = {}
        # Create nonce and add to session nonces list
        nonce = self.session['nonce_manager'].generate_nonce(self.request)
        offset = 0
        while nonce in self.session['nonces']:
            offset += 1
            nonce = self.session['nonce_manager'].generate_nonce(self.request, offset)

        dumpvar = self.session['nonces'].copy()
        dumpvar[nonce] = int(datetime.datetime.now().timestamp())
        # Cleanup old nonces
        expired = int((datetime.datetime.now() - datetime.timedelta(minutes=120)).timestamp())
        for each in self.session['nonces']:
            if dumpvar[each] < expired:
                del dumpvar[each]
        self.session['nonces'] = dumpvar.copy()
        csrf += nonce
        return csrf

    def _verify_csrf(self, name, passed_in):
        csrf = None
        if "csrf" + name in self.session:
            csrf = self.session["csrf" + name]
        if csrf is None or passed_in is None or passed_in[:len(csrf)] != csrf:
            return False
        verified = False
        # Get the nonce and check validity and that it was generated for this session
        dumpvar = self.session.get('nonces')
        if dumpvar is not None:
            dumpvar = dumpvar.copy()
            nonce = passed_in[-(len(passed_in) - len(csrf)):]
            if nonce in dumpvar and \
                    self.nonce_manager.is_valid_nonce(nonce, self.request):
                # Remove the nonce from the useable list so it can't be replayed
                del dumpvar[nonce]
                self.session['nonces'] = dumpvar
                verified = True
        return verified

    def _validate(self, action):
        self.params = self.request.POST or self.request.params
        self.error = None
        if not self._verify_csrf(action, self.params.get("csrf")):
            self.error = {"message": "CSRF failure"}
            return False
        return True

    @JSONResponse
    def error_page(self):
        return self.error


class LoggedInHandler(BaseHandler):
    pass
