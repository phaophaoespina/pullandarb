from handlers.base import BaseHandler
from bottle import jinja2_view
from bottle import redirect, response, request
from factories.base import (
    UniversityFactory,
    UserUniversityMapFactory,
    AdminUserMapFactory,
    UserFactory,
    UserSettingsFactory
)
from lib.helpers import settings
from sqlalchemy.exc import IntegrityError
from lib import validation
from functools import wraps
import uuid
import validators
import csv
import logging
import json
from handlers.base import JSONResponse
import importlib
from formencode import Invalid
from password_strength import PasswordPolicy
from lib.axieinfinity import Axie
import locale

policy = PasswordPolicy.from_names(
    length=8,  # min length: 8
    uppercase=1,  # need min. 2 uppercase letters
    nonletters=1,  # need min. 2 non-letter characters (digits, specials, anything)
)

from collections import defaultdict
import datetime

EXCLUDED_CENSOR = []


class PublicViews(BaseHandler):
    response = None
    params = None
    user_factory = None

    def __init__(self, session, action):
        self.user_factory = UserFactory()
        super(PublicViews, self).__init__(session, action)

    @jinja2_view('login.html')
    def login(self):
        if self.session['logged_in'] is not None:
            logging.info("Logged in already")
            response.status = 303
            response.set_header('Location', '/home')
            return {}
        login2fa_csrf = self._set_csrf("login2fa")
        login_csrf = self._set_csrf("login")
        return {'login_csrf': login_csrf, "login2fa_csrf": login2fa_csrf}

    @JSONResponse
    def login2fa_post(self):
        user_uuid = self.params['user']
        landing_page = self.params['page']
        otp = self.params['otp']
        user = self.user_factory.get_by_attribute('uuid', user_uuid)
        valid = self.user_factory.verify_otp(otp, user.secret_key)

        if valid:
            self.session['logged_in'] = user.uuid
            self.session['authority'] = user.authority.lower()
            return {"page": landing_page}
        else:
            return {"message": "Failed to authenticate. OTP code is invalid."}

    @jinja2_view('register.html')
    def register(self):
        register_csrf = self._set_csrf("register")
        return {'register_csrf': register_csrf}

    @JSONResponse
    def register_post(self):
        username = self.params['u']
        password = self.params['p']
        if len(username) < 1:
            return {"message": "Unable to create user. Username field cannot be empty.", "page": "/login"}
        if len(password) < 1:
            return {"message": "Unable to create user. Password field cannot be empty.", "page": "/login"}
        policycheck = policy.test(password)
        if policycheck != []:
            return {
                "message": "Unable to create user. Password must be at least 8 characters, 1 capital letter,"
                           " and 1 non-letter character", "page": "/login"}
        found = self.user_factory.get_by_attribute("username", username)
        if found:
            return {"message": "User with that username already exists, try another one"}

        user = self.user_factory.create(username, 'admin', password, "Active")
        if user is not None:
            return {"message": "User successfully created. You may now login", "page": "/login"}

        return {"message": "Registration failed"}


    @JSONResponse
    def login_post(self):
        username = self.params['u']
        password = self.params['p']
        # TODO verify user and then route to admin or regular screen based on credential
        user = self.user_factory.check_user_credentials(username, password)
        if user is not None:
            if user.auth_status == "Disabled" or user.auth_status is None:
                self.session['logged_in'] = user.uuid
                self.session['authority'] = user.authority.lower()

            if user.authority.lower() == "admin":
                return {"page": "/admin", "auth": user.auth_status, "user": user.uuid}
            elif user.authority.lower() == "manager":
                return {"page": "/dashboard", "auth": user.auth_status, "user": user.uuid}
            elif user.authority.lower() == "scholar":
                if user.status == "Active":
                    return {"page": "/dashboard", "auth": user.auth_status, "user": user.uuid}
                elif user.status == "Inactive":
                    del self.session['logged_in']
                    del self.session['authority']
                    return {"message": "Your account has been disabled."}
            else:
                {"message": "Error: role is undefined"}
        return {"message": "Your username or password is incorrect"}

    '''@jinja2_view('register.html')
    def register(self):
        csrf = self._set_csrf("register")
        return {'csrf': csrf}

    @jinja2_view('message.html')
    def register_post(self):
        return {"message": "Successfully registered"}'''


def is_admin(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        self = args[0]
        if self.session.get('authority') == 'admin':
            return f(*args, **kwargs)
        redirect("/home")

    return wrapper


class AuthorizedViews(BaseHandler):
    response = None
    params = None
    resource = None
    adminuser_factory = None

    def __init__(self, session, action, resource):
        if session.get("logged_in") is None:
            # check session to see if logged in. If not, kick them back to login screen
            redirect("/login")

        try:
            # If resource is a UUID, doesn't belong as part of the action
            uuid.UUID(resource)
            self.resource = resource
        except Exception as e:
            logging.info("Resource error: {}".format(e))
            if resource is None:
                resource = ''
            else:
                resource = "_" + resource
            # The action corresponds to function name.
            action = action + resource

        self.user_factory = UserFactory()
        self.user_university_map_factory = UserUniversityMapFactory()
        self.university_factory = UniversityFactory()
        self.user_settings_factory = UserSettingsFactory()
        self.axie_infinity = Axie()
        self.adminuser_factory = AdminUserMapFactory()

        super(AuthorizedViews, self).__init__(session, action)

    # def _get_exchange_helpers(self):
    #     """
    #     Get all Exchanges and their validators
    #     :return: A dict with 'validator' and 'client' for each of the exchanges by name
    #     """
    #     exchanges = {}
    #     for _ in EXCHANGES:
    #         name = EXCHANGES[_].replace(" ", "")
    #         if name == "Ethereum":
    #             module = importlib.import_module("lib.ethereum")
    #         elif name == "Axie":
    #             module = importlib.import_module("lib.axieinfinity")
    #         elif name.startswith("Coinbase"):
    #             module = importlib.import_module("exchanges.coinbase")
    #         elif name.startswith("Binance"):
    #             module = importlib.import_module("exchanges.binance")
    #         else:
    #             module = importlib.import_module("exchanges." + _)
    #         exchanges[_] = {"validator": getattr(validation, name + "Validator"),
    #                         "client": getattr(module, name)}
    #     return exchanges

    def __university_list(self, user):
        university_filter = [
            self.university_factory.type.created_by == user
        ]
        universities = self.university_factory.get(filters=university_filter, limit=100)
        university_list = list()
        for university in universities:
            university_list.append({"name": university.name, "uuid": university.uuid})
        return sorted(university_list, key=lambda _: _['name'])

    def __university_list_per_employee(self, user):
        university_list = list()
        mappings_found = self.user_university_map_factory.retrieve_all_user_mappings(user)
        if mappings_found:
            for mapping in mappings_found:
                university = self.university_factory.get_by_attribute("uuid", mapping.university_uuid)
                if university:
                    university_list.append({"name": university.name, "uuid": university.uuid})

        return sorted(university_list, key=lambda _: _['name'])

    def __university_list_per_client(self, user):
        university_list = list()
        mappings_found = self.user_university_map_factory.retrieve_all_user_mappings(user)
        if mappings_found:
            for mapping in mappings_found:
                university = self.university_factory.get_by_attribute("uuid", mapping.university_uuid)
                if university:
                    university_list.append({"name": university.name, "uuid": university.uuid})

        return sorted(university_list, key=lambda _: _['name'])

    def __address_details(self, addresses):

        self.axie_infinity.load_addresses(addresses)
        self.axie_infinity.get_address_data()
        addresses_data = self.axie_infinity.get_latest_addresses_data()
        timeseries_data = self.axie_infinity.get_all_adddress_data()

        return addresses_data, timeseries_data

    def __axie_accounts_stats(self, addresses):

        slp_price_php = self.axie_infinity.get_slp_latest_price()
        profit_loss_data = self.axie_infinity.get_latest_profit_loss_data(addresses, slp_price_php['php'])

        return slp_price_php, profit_loss_data

    def __axie_scholars_performance(self, data):

        perf_data = self.axie_infinity.get_daily_slp(data)
        users = [d['user'] for d in perf_data]

        return perf_data, users

    @is_admin
    @jinja2_view('admin.html')
    def admin(self):
        """ This view allows administrative accounts to see all university accounts """
        user_csrf = self._set_csrf("user_create")
        user_update_csrf = self._set_csrf("user_update")
        user_set_status_csrf = self._set_csrf("setstatus")
        university_csrf = self._set_csrf("university_create")
        university_delete_csrf = self._set_csrf("universitydelete")
        university_update_csrf = self._set_csrf("university_update")
        user_delete_csrf = self._set_csrf("userdelete")
        user_edit_csrf = self._set_csrf("useredit")
        mapping_csrf = self._set_csrf("mapping_create")
        token_csrf = self._set_csrf("token_whitelist")
        enable_disable_2fa_csrf = self._set_csrf("enable_disable_2fa")
        fa_filters = [
            self.user_factory.type.auth_status == "Enabled",
            self.user_factory.type.uuid == self.session['logged_in']
        ]

        enabled_2fa = self.user_factory.get(filters=fa_filters, limit=1)
        token_delete_csrf = self._set_csrf("token_delete")
        update_user_university_mapping_csrf = self._set_csrf("update_user_university_mapping")
        university_list = self.__university_list(self.session['logged_in'])
        admin_list = self.user_factory.get_user_list_by_authority(u'admin')
        employee_list = self.user_factory.get_user_list_by_authority(u'manager')
        scholar_list = self.user_factory.get_user_list_by_authority(u'scholar')

        non_admins = employee_list + scholar_list

        employees_of_user = list()
        for user in employee_list:
            print("USERS: ")
            print(user)
            if self.adminuser_factory.retrieve_mapping(self.session['logged_in'], user['uuid']):
                employees_of_user.append(user)

        scholar_of_user = list()
        for user in scholar_list:
            print("USERS: ")
            print(user)
            if self.adminuser_factory.retrieve_mapping(self.session['logged_in'], user['uuid']):
                scholar_of_user.append(user)

        non_admins_of_user = list()
        for user in scholar_list:
            print("USERS: ")
            print(user)
            if self.adminuser_factory.retrieve_mapping(self.session['logged_in'], user['uuid']):
                non_admins_of_user.append(user)

        mappings = list()

        for university in university_list:
            for admin_user in admin_list:
                if self.adminuser_factory.retrieve_mapping(self.session['logged_in'], admin_user['uuid']):
                    admin_mapping_exists = self.user_university_map_factory.retrieve_mapping(university['uuid'],
                                                                                             admin_user['uuid'])
                    if not admin_mapping_exists:
                        self.user_university_map_factory.create(admin_user['uuid'], university['uuid'])

        user_and_admins = non_admins + admin_list
        for user in user_and_admins:
            companies = list()
            if self.adminuser_factory.retrieve_mapping(self.session['logged_in'], user['uuid']):
                mappings_found = self.user_university_map_factory.retrieve_all_user_mappings(user['uuid'])
                if mappings_found:
                    for mapping in mappings_found:
                        university = self.university_factory.get_by_attribute("uuid", mapping.university_uuid)
                        if university:
                            companies.append({"name": university.name, "uuid": university.uuid})

            mapping = {
                "user": user,
                "companies": companies,
            }
            mappings.append(mapping)

        return {"user_csrf": user_csrf, "university_csrf": university_csrf, "mapping_csrf": mapping_csrf,
                "universities": university_list, "users": non_admins + admin_list, "user_edit_csrf": user_edit_csrf,
                "university_delete_csrf": university_delete_csrf, "user_set_status_csrf": user_set_status_csrf,
                "university_update_csrf": university_update_csrf, 'token_csrf': token_csrf,
                "token_delete_csrf": token_delete_csrf,
                "user_update_csrf": user_update_csrf, "admins": admin_list, "employees": employees_of_user,
                "clients": scholar_of_user, "non_admins": non_admins_of_user, "mappings": mappings,
                "user_delete_csrf": user_delete_csrf, "enable_disable_2fa_csrf": enable_disable_2fa_csrf,
                "update_user_university_mapping_csrf": update_user_university_mapping_csrf, "enabled_2fa": enabled_2fa}

    @is_admin
    @JSONResponse
    def mapping_create_post(self):
        try:
            user_uuid = self.params['user_uuid']
            university_uuid = self.params['university_uuid']
        except KeyError as e:
            return {"message": "Error: {}".format(e)}
        user = self.user_factory.get_by_attribute("uuid", user_uuid)
        university = self.university_factory.get_by_attribute("uuid", university_uuid)

        mapping_exists = self.user_university_map_factory.retrieve_mapping(university_uuid, user_uuid)
        if mapping_exists:
            return {"message": "{} to {} mapping already exists".format(user.username, university.name)}
        try:
            mapping = self.user_university_map_factory.create(user.uuid, university.uuid)
            if mapping:
                return {"message": "Mapped User {} to University {}".format(user.username, university.name)}
        except Exception as e:
            return {"message": "Mapping error {}".format(e)}

    @jinja2_view('home.html')
    def home(self):

        _uuid = self.resource
        if self.session.get("authority") != "scholar" and self.resource is None:
            redirect("/admin")
        elif _uuid is None:
            mapping = self.user_university_map_factory.get_by_attribute("user_uuid", self.session['logged_in'])
            if mapping is None:
                redirect("/login")
            _uuid = mapping.university_uuid

        scholars_list = self.user_factory.get_user_list_by_authority(u'scholar')

        scholars_in_university = list()

        for user in scholars_list:
            # if self.adminuser_factory.retrieve_mapping(self.session['logged_in'], user['uuid']):
            if user['status'] == 'Active' and user['ronin_address'] != None:
                mapping_exists = self.user_university_map_factory.retrieve_mapping(_uuid, user['uuid'])
                if mapping_exists:
                    ronin_addr = user['ronin_address']
                    if ronin_addr.startswith('ronin'):
                        ronin_addr = user['ronin_address'].replace("ronin:", "0x")
                    scholars_in_university.append({'ronin': ronin_addr, 'user_uuid': user['uuid']})

        scholar_list_normal = list()
        for scholar in scholars_in_university:
            user = self.user_factory.get_by_id(scholar['user_uuid'])
            scholar_list_normal.append(user)

        university = self.university_factory.get_by_attribute("uuid", _uuid)
        csrf = self._set_csrf("home")

        if self.session["authority"] == "admin":
            university_list = self.__university_list(self.session['logged_in'])
        elif self.session["authority"] == "scholar":
            university_list = self.__university_list_per_client(self.session['logged_in'])
        elif self.session["authority"] == "manager":
            university_list = self.__university_list_per_employee(self.session['logged_in'])
        else:
            return {"message": "role not found"}

        ronin_addresses_data, timeseries_data = self.__address_details(scholars_in_university)

        slp_price, profit_loss_data = self.__axie_accounts_stats(ronin_addresses_data)
        profit_loss_count = [i['profit_loss_calculation'] for i in profit_loss_data]
        slp_total_cash = [i['slp_cash_to_date'] for i in profit_loss_data]
        scholars_performance, users_in_perf = self.__axie_scholars_performance(timeseries_data)
        print(scholars_performance)
        print(users_in_perf)

        return {'csrf': csrf, 'uuid': _uuid,
                'university': university,
                'universities': university_list,
                'scholar_list_normal': scholar_list_normal,
                'role': self.session["authority"],
                'ronin_addresses_data': ronin_addresses_data,
                'slp_price': slp_price['php'],
                'slp_price_usd': slp_price['usd'],
                'profit_loss_data': profit_loss_data,
                'profit_loss_count': '{0:,.2f}'.format(round(sum(profit_loss_count), 2)),
                'slp_total_cash': '{0:,.2f}'.format(round(sum(slp_total_cash), 2)),
                'scholars_performance': json.dumps(scholars_performance, indent=4, sort_keys=True, default=str),
                'users_in_perf': json.dumps(users_in_perf, indent=4, sort_keys=True, default=str)
                }

    @jinja2_view('dashboard.html')
    def dashboard(self):
        # TODO -- enable when endpoint is made
        university_list = list()
        if self.session['authority'] == "manager":
            university_list = self.__university_list_per_employee(self.session['logged_in'])

        elif self.session['authority'] == "scholar":
            university_list = self.__university_list_per_client(self.session['logged_in'])

        elif self.session['authority'] == "admin":
            redirect("/admin")
        else:
            university_list = list()

        print("UNIVERSITY LIST:  ")
        print(university_list)
        user_update_csrf = self._set_csrf("user_update")
        usr_details = self.user_factory.get_by_id(self.session['logged_in'])
        if usr_details.ronin_address:
            usr_details.ronin_address = "https://marketplace.axieinfinity.com/profile/{}/axie" \
                .format(usr_details.ronin_address.replace('0x', 'ronin:'))

        fa_filters = [
            self.user_factory.type.auth_status == "Enabled",
            self.user_factory.type.uuid == self.session['logged_in']
        ]

        enable_disable_2fa_csrf = self._set_csrf("enable_disable_2fa")
        enabled_2fa = self.user_factory.get(filters=fa_filters, limit=1)
        return {"universities": university_list, "user_update_csrf": user_update_csrf,
                "role": self.session["authority"],
                "enabled_2fa": enabled_2fa, "enable_disable_2fa_csrf": enable_disable_2fa_csrf,
                'usr_details': usr_details}

    @is_admin
    @JSONResponse
    def user_create_post(self):
        username = self.params['username']
        password = self.params['password']
        if len(username) < 1:
            return {"message": "Unable to create user. Username field cannot be empty.", "target": "/admin#users"}
        if len(password) < 1:
            return {"message": "Unable to create user. Password field cannot be empty.", "target": "/admin#users"}
        policycheck = policy.test(password)
        if policycheck != []:
            return {
                "message": "Unable to create user. Password must be at least 8 characters, 1 capital letter,"
                           " and 1 non-letter character", "target": "/admin#users"}
        found = self.user_factory.get_by_attribute("username", username)
        if found:
            found_final = self.adminuser_factory.retrieve_mapping(self.session['logged_in'], found.uuid)
            if found_final is not None:
                return {"message": "User with that username already exists"}

        user = self.user_factory.create(username, self.params['level'].lower(), password, "Inactive")
        self.adminuser_factory.create(self.session['logged_in'], user.uuid)
        if user is not None:
            return {"message": "User successfully created", "target": "/admin#users"}
        return {"message": "Unable to create user"}

    @JSONResponse
    def user_update_post(self):
        """
        Updates user
        Just password for now
        """
        user = self.user_factory.get_by_id(self.session['logged_in'])
        if user.validate(self.params['current']):
            if self.params['new'] != self.params['confirm']:
                return {"message": "Passwords do not match"}
            user.set_password(self.params['new'])
            obj = self.user_factory.update(user)
            if obj is not None:
                return {"message": "User successfully updated"}
        return {"message": "Unable to update user information"}

    @is_admin
    @JSONResponse
    def useredit_post(self):
        """
        Updates user profile
        """
        university_uuids = self.params['university_uuids[]'].strip().split(',')
        user_uuid = self.params['user_uuid'].strip()
        new_username = self.params['edit-username'].strip()
        new_password = self.params['edit-password'].strip()
        new_status = self.params['edit-status'].strip()

        try:
            new_app_qr = self.params['edit-qr']
        except KeyError:
            new_app_qr = ''
        except Exception as e:
            print(e)
            raise e
        print(new_app_qr)

        try:
            new_app_level = self.params['edit-level'].strip()
        except KeyError:
            new_app_level = ''

        try:
            new_app_login = self.params['edit-app-login'].strip()
        except KeyError:
            new_app_login = ''

        try:
            new_app_password = self.params['edit-app-password'].strip()
        except KeyError:
            new_app_password = ''

        try:
            new_ronin_address = self.params['edit-ronin-address'].strip()
        except KeyError:
            new_ronin_address = ''

        try:
            new_scholar_share = self.params['edit-scholar-share'].strip()
        except KeyError:
            new_scholar_share = ''

        try:
            new_team_aquisition = self.params['edit-team-aquisition-cost'].strip()
        except KeyError:
            new_team_aquisition = ''

        try:
            new_team_current_valuation = self.params['edit-team-current-valuation'].strip()
        except KeyError:
            new_team_current_valuation = ''

        if new_password:
            policycheck = policy.test(new_password)
            if policycheck != []:
                return {
                    "message": "Unable to create user. Password must be at least 8 characters, 1 capital letter,"
                               " and 1 non-letter character", "target": "/admin#users"}
        user = self.user_factory.get_by_attribute("uuid", user_uuid)
        if user is None:
            return {"message": "User object not found"}

        if new_username and new_password:
            user.set_password(new_password)
            user.username = new_username

        # user.status = new_status
        if new_app_qr != '':
            user.qr_data = new_app_qr

        if new_app_level != '':
            user.authority = new_app_level

        if new_team_aquisition != '':
            user.team_aquisition_cost = new_team_aquisition

        if new_team_current_valuation != '':
            user.team_current_valuation = new_team_current_valuation

        if new_app_login != '':
            user.app_login = new_app_login

        if new_scholar_share != '':
            user.scholar_share = new_scholar_share

        if new_app_password != '':
            user.app_password = new_app_password

        if new_ronin_address != '':
            ronin_addr = new_ronin_address

            if not new_ronin_address.startswith('ronin'):
                return {"message": "Bad format: Ronin Address, please enter a valid address"}

            if ronin_addr.startswith('ronin'):
                ronin_addr = new_ronin_address.replace("ronin:", "0x")

                found = self.user_factory.get_by_attribute("ronin_address", ronin_addr)
                if found is not None:
                    return {"message": "Scholar with that ronin address already exists"}

            user.ronin_address = ronin_addr

        # user.qr_code = new_app_qr
        obj = self.user_factory.update(user)

        if obj is not None:

            found_user_mappings = self.user_university_map_factory.retrieve_all_user_mappings(user_uuid)

            if found_user_mappings:
                for mapping in found_user_mappings:
                    mapping_uuid = mapping.uuid
                    self.user_university_map_factory.delete(mapping_uuid)

            for university_uuid in university_uuids:
                if university_uuid:
                    mapping_exists = self.user_university_map_factory.retrieve_mapping(university_uuid, user_uuid)
                    if not mapping_exists:
                        self.user_university_map_factory.create(user_uuid, university_uuid)
            return {"message": "User successfully updated", "target": "/admin#users"}
        else:
            return {"message": "User update failed"}

    @is_admin
    @JSONResponse
    def userdelete_post(self):
        """
        Delete user as an admin
        :return: Message as to whether it was a success or not
        """
        try:
            user_uuid = self.params['user_uuid']
            if user_uuid == self.session['logged_in']:
                raise Invalid('You cannot delete yourself!')
        except Invalid as error:
            return json.dumps(error.unpack_errors())

        # Search for existing mappings
        logged_in_user_authority = self.session['authority']
        found_user_mappings = self.user_university_map_factory.retrieve_all_user_mappings(user_uuid)
        found_user_settings = self.user_settings_factory.get_all_by_attribute("created_by", user_uuid)

        if found_user_settings:
            self.user_settings_factory.delete_all(found_user_settings)

        delete_status = dict()
        user_mappings_deletions = list()

        if logged_in_user_authority.lower() == "admin":
            if found_user_mappings:
                for mapping in found_user_mappings:
                    mapping_uuid = mapping.uuid
                    user_mapping_deletion = self.user_university_map_factory.delete(mapping_uuid)
                    user_mappings_deletions.append(user_mapping_deletion)
            if user_uuid:
                deletion_map = self.user_university_map_factory.delete(user_uuid)
                deletion_user = self.user_factory.delete(user_uuid)
                delete_status['map'] = deletion_map
                delete_status['user'] = deletion_user

            delete_status['user_map'] = user_mappings_deletions

            return {"status": True, "message": "Success: User successfully deleted."}
        else:
            return {"status": True, "message": "Failed: Invalid authority level"}

    @is_admin
    @JSONResponse
    def setstatus_post(self):
        try:
            username = self.params['username']
            status = self.params['status']
        except KeyError as e:
            return {"message": "Error. Missing {}".format(e)}

        users = self.user_factory.get_all_by_attribute("username", username)
        for user in users:
            if self.adminuser_factory.retrieve_mapping(self.session['logged_in'], user.uuid):
                if user.authority == "scholar":
                    if status == "Inactive":
                        user.status = "Active"
                    elif status == "Active":
                        user.status = "Inactive"
                    message = self.user_factory.update(user)
                    return {"message": "{} set to {}".format(message.username, message.status)}
                else:
                    return {"message": "Failed to set status, user is not a Client"}

    @is_admin
    @JSONResponse
    def university_create_post(self):
        """
        Create university as an admin
        :return: Message as to whether it was a success or not
        """
        # Validation
        try:
            if not validators.length(self.params['university'], min=3):
                return {"message": "University name must be 3 characters"}
        except KeyError:
            return {"message": "Must submit university name"}
        else:
            university = self.params['university']
        found = self.university_factory.get_by_attribute("name", university)
        if found is None:
            obj = self.university_factory.create(university, self.session['logged_in'])
        else:
            return {"message": "University already exists"}
        # End validation

        # UNUSED - exchanges variable unused
        # exchanges = self.exchange_factory.get(limit=100)

        if obj is not None:
            return {"message": "University created!"}

        return {"message": "error creating exchange"}

    @is_admin
    @JSONResponse
    def universitydelete_post(self):
        """
        Delete university as an admin
        :return: Message as to whether it was a success or not
        """
        try:
            university_uuid = self.params['university_uuid']
        except Invalid as error:
            logging.exception(json.dumps(error.unpack_errors()))
            return {"status": False, "message": "Failure: An Invalid university uuid has been detected, please contact"
                                                "administrator"}
        found_user_mappings = self.user_university_map_factory.get_all_by_attribute("university_uuid", university_uuid)
        found_user_settings = self.user_settings_factory.get_all_by_attribute("university_uuid", university_uuid)

        if found_user_settings:
            self.user_settings_factory.delete_all(found_user_settings)

        if found_user_mappings:
            for mapping in found_user_mappings:
                mapping_uuid = mapping.uuid
                self.user_university_map_factory.delete(mapping_uuid)

        if university_uuid:
            self.university_factory.delete(university_uuid)
        else:
            return {"status": False, "message": "Failure: University id doesnt exist, please contact administrator"}

        return {"status": True, "message": "Success: University successfully deleted."}

    @is_admin
    @JSONResponse
    def update_user_university_mapping_post(self):
        university_uuids = self.params['university_uuids[]'].split(',')
        user_uuid = self.params['user_uuid']
        found_user_mappings = self.user_university_map_factory.retrieve_all_user_mappings(user_uuid)
        if found_user_mappings:
            for mapping in found_user_mappings:
                mapping_uuid = mapping.uuid
                self.user_university_map_factory.delete(mapping_uuid)
        for university_uuid in university_uuids:
            if university_uuid:
                mapping_exists = self.user_university_map_factory.retrieve_mapping(university_uuid, user_uuid)
                if not mapping_exists:
                    self.user_university_map_factory.create(user_uuid, university_uuid)

        return {"message": "done"}

    def logout(self):
        del self.session['logged_in']
        del self.session['authority']
        response.status = 303
        response.set_header('Location', '/login')
        return {}

    @is_admin
    @JSONResponse
    def university_update_post(self):
        """
        Updates university name
        """
        university_uuid = self.params['university_uuid']
        university_name = self.params['university_name']

        try:
            if not validators.length(university_name, min=3):
                return {"message": "University  name must be 3 characters or more."}
        except KeyError:
            return {"message": "Please enter new university name"}

        universityname = self.university_factory.get_by_attribute("name", university_name)
        if universityname is None:
            university = self.university_factory.get_by_attribute("uuid", university_uuid)
            university.name = university_name
            obj = self.university_factory.update(university)
        else:
            return {"message": "University already exists, please input other university name."}

        if obj is not None:
            return {"message": "University name successfully updated!"}

        return {"message": "Something went wrong, unable to update university name."}

    @is_admin
    @JSONResponse
    def save_settings_post(self):
        """
        create settings
        """

        created_by = self.session['logged_in']
        university_uuid = self.params['university_uuid']
        setting = self.params['setting']
        setting_filters = [
            self.user_settings_factory.type.university_uuid == university_uuid,
            self.user_settings_factory.type.created_by == created_by
        ]
        setting_found = self.user_settings_factory.get(filters=setting_filters)
        if not setting_found:
            self.user_settings_factory.create(created_by, university_uuid, setting)
            return {"status": False, "message": "Configuration was successfully saved!", "target": "/home"}
        else:
            setting_found[0].settings = setting
            self.user_settings_factory.update(setting_found[0])
            return {"status": False, "message": "Transaction table configuration was successfully updated",
                    "target": "/home"}

    @JSONResponse
    def enable_disable_2fa_post(self):
        user = self.session['logged_in']
        status = self.params['auth_status']
        user_found = self.user_factory.get_by_attribute("uuid", user)

        if user_found.secret_key is None:
            secret_key = self.user_factory.create_secret_key()
            user_found.secret_key = secret_key
            user_found.auth_status = status
            self.user_factory.update(user_found)
        else:
            user_found.auth_status = status
            self.user_factory.update(user_found)

        return {"status": status, "secret_key": user_found.secret_key}
