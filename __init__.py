from app import main


if __name__ == '__main__':
    main()


from ._version import get_versions
__version__ = get_versions()['version']
del get_versions
