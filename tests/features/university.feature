Feature: There is an admin page where companies can be added and linked to

  Background:
    Given I am logged in
    Given I am on the admin page
    Then The title should be Axie Iskolarship - Management Dashboard

  Scenario: There is a form for creating new companies
    When I click the AddNewUniversity element
    Then I wait 1 second
    Then The newUniversity form's 1 field is university
    Then There is a SUBMIT button on the newUniversity form

