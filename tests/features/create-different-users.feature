Feature: There is an admin page where companies can be added and linked to

  Background:
    Given I am logged in
    Given I am on the admin page
    When I click the users-tab element

  Scenario: Visit the admin page
    Then The title should be Axie Iskolarship - Management Dashboard

  Scenario: There is a form for creating new users

    Then The createNewUser form's 1 field is username
    Then The createNewUser form's 2 field is password
    Then The createNewUser form's 3 field is csrf
    Then There is a SUBMIT button on the createNewUser form
