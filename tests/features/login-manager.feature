Feature: There is a page where client users can add connections and produce reports
  Background:
    Given I am on the login page
    Given I logged in as employee

  Scenario: Navbar and logout elements exists
    Given I am on the dashboard page
    Then The element settings-link does exist
    Then I click on the settings tab
    Then The element current-pw does exist
    Then The element new-pw does exist
    Then The element confirm-pw does exist
    Then The element confirm-pw does exist
    Then The css button.btn element does exist
    Then logout button exists
    Then I wait 1 second

