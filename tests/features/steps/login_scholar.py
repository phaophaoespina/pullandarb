from behave import given, when
from tests.features.environment import browse_to

username = 'admin'
password = 'LrP72x7Fmz6xWda2'


@when("I log in a client")
def login(context):
    context.execute_steps(u"""
       given I am on the login page
         when valid client credentials are entered
         and I click the submit button 
    """)


@given("I logged in as client")
def login(context):
    browse_to(context, "login")
    if context.browser.title == 'Login':
        context.execute_steps(u"""when I log in a client""")




