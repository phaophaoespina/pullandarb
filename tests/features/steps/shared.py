import time

from behave import given, when, then
import uuid
import logging
import random

from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from time import sleep
from tests.features.environment import \
    browse_to, wait_element_presence, wait_element_visibility, \
    wait_element_clickability, wait_element_retry_and_click
import transaction
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import engine_from_config
from models import User, Exchange
from sqlalchemy.ext.declarative import declarative_base
from lib.helpers import settings


@given("I am on the {pagename} page")
def go_to_the_page(context, pagename):
    browse_to(context, pagename)


@when("I send data {data} into the {name} name element")
def enter_data(context, data, name):
    wait_element_presence(context.browser, By.NAME, name).send_keys(data)


@when("I enter {data} into the {inputname} text input")
def enter_data_text_input(context, data, inputname):
    context.browser.find_element_by_id(inputname).send_keys(data)


@when("I set the value of {inputname} to {data}")
def enter_data_text_input(context, inputname, data):
    context.browser.find_element_by_id(inputname).value = data
    context.browser.find_element_by_id(inputname).required = False


@when("I scroll to top of page")
def scroll_to_top(context):
    # Top of Page
    context.browser.execute_script("window.scrollTo(0, 0);")


@when("I scroll to {id} element")
def scroll_to(context, id):
    scroll_to_top(context)
    # Scroll into view
    element = context.browser.find_element_by_id(id)
    context.browser.execute_script("return arguments[0].scrollIntoView(true);", element)


@when("I click the {btnid} button")
def click_the_button(context, btnid):
    wait_element_clickability(context.browser, By.ID, btnid).click()


@when("I click the name {name} element")
def click_the_button(context, name):
    context.browser.find_element_by_name(name).click()


@when("I click the class {classname} element")
def click_the_button(context, classname):
    context.browser.find_element_by_class_name(classname).click()


@when("I click the {id} element")
def click_the_button(context, id):
    wait_element_retry_and_click(context.browser, By.ID, id)


@when("I select the {number} item from the {id} dropdown")
def select_dropdown_item(context, number, id):
    context.browser.find_element_by_id(id).click()
    actions = ActionChains(context.browser)
    arrow_downs = int(number)
    for i in range(1, arrow_downs):
        actions.send_keys(Keys.ARROW_DOWN)
    actions.click()
    actions.perform()


@when("I select the text {text} from the company dropdown")
def select_company_dropdown_by_text(context, text):
    select_xpath = '//div[@id="selectCompany"]'
    li_xpath = '//div[@id="selectCompany"]//li[span[text()="%s"]]' % text
    wait_element_retry_and_click(context.browser, By.XPATH, select_xpath)
    wait_element_retry_and_click(context.browser, By.XPATH, li_xpath)


@when("I select the edit user from the client dropdown")
def select_company_dropdown_by_text(context):
    select_xpath = 'td.center:nth-child(4) > a:nth-child(1)'
    li_xpath = '//td[@id="client_dropdown"]//li[a[text()="Edit User"]]'
    wait_element_retry_and_click(context.browser, By.CSS_SELECTOR, select_xpath)
    wait_element_retry_and_click(context.browser, By.XPATH, li_xpath)


@when("I click dropdown element with role: Employee")
def select_tr_by_text(context):
    select_xpath = '//tr[td//text()[contains(., "Employee")]]//a[@class="dropdown-trigger"]'
    li_xpath = '//td[@id="employee_dropdown"]//li[a[text()="Edit User"]]'
    wait_element_retry_and_click(context.browser, By.XPATH, select_xpath)
    wait_element_retry_and_click(context.browser, By.XPATH, li_xpath)


@when("I assign a company")
def select_company_dropdown_by_text(context):
    input_xpath = context.browser.find_element_by_xpath(
        '(//table[@ id="user_company_table"]//tbody/tr/td[2]//span)[1]')
    input_xpath.click()


@when("I select the text {text} from the reports dropdown")
def select_user_dropdown_by_text_type(context, text):
    select_div = context.browser.find_element_by_id('report-type-select-div')
    list_items = select_div.find_elements_by_tag_name('li')
    select_div.click()
    for list_item in list_items:
        inner_text = list_item.get_attribute('innerText')
        if "transactions" in inner_text:
            list_item.click()

    select_div_connection = context.browser.find_element_by_id('report-connection-select-div')
    list_items_connection = select_div_connection.find_elements_by_tag_name('li')
    select_div_connection.click()
    for list_item_connect in list_items_connection:
        inner_text = list_item_connect.get_attribute('innerText')
        if text in inner_text:
            list_item_connect.click()


@when("I select the text {text} from the user dropdown")
def select_user_dropdown_by_text(context, text):
    select_xpath = '//div[@id="selectUser"]'
    li_xpath = '//div[@id="selectUser"]//li[span[text()="%s"]]' % text
    wait_element_retry_and_click(context.browser, By.XPATH, select_xpath)
    wait_element_retry_and_click(context.browser, By.XPATH, li_xpath)


@when("I click the list item element {connection}")
def click_the_list_item(context, connection):
    list_items = context.browser.find_elements_by_tag_name('li')
    for item in list_items:
        found = item.get_attribute('innerText')
        if found == connection:
            item.click()
            break


@then("The list item {item} exists")
def click_the_list_item(context, item):
    list_items = context.browser.find_elements_by_tag_name('li')
    matched = False
    for list_item in list_items:
        found = list_item.get_attribute('innerText')
        found = str(found).strip()
        if found == str(item):
            matched = True

    assert matched


@when("I type {data} into the {id} element and press enter")
def id_type_enter(context, data, id):
    context.browser.find_element_by_id(id).click()
    context.browser.find_element_by_id(id).send_keys(data)
    context.browser.find_element_by_id(id).send_keys(Keys.ENTER)


@when("I type {data} into the {id} element")
def id_type(context, data, id):
    wait_element_presence(context.browser, By.ID, id, 30).send_keys(data)


@then("The title should be {title}")
def title_check(context, title):
    wait_element_presence(context.browser, By.TAG_NAME, 'title')
    x = context.browser.title
    assert x == title


@then("The {form} form's {num:d} field is {inputname}")
def input_check(context, inputname, num, form):
    x = context.browser.find_element_by_xpath("//form[@id='%s']/input[%d]" % (form, num))
    y = x.get_attribute('name')
    assert y == inputname


@then("There is a {btnname} button on the {form} form")
def button_check(context, btnname, form):
    x = context.browser.find_element_by_xpath("//form[@id='%s']/div/button" % form)
    assert x.text == btnname


@then("There is a message that says {msg}")
def message_says(context, msg):
    x = context.browser.find_element_by_id("message")
    assert x.text == msg


@then("There is a modal with a message that says {msg}")
def message_says_modal(context, msg):
    y = wait_element_visibility(context.browser, By.ID, "result_modal", 60)
    assert y.is_displayed() is True
    x = wait_element_visibility(context.browser, By.ID, "result_modal_text")
    x_string_ = x.get_attribute('innerText')
    x_string = x_string_.strip()
    assert x_string == msg


@then("There is a modal that prompts a message that starts with {msg}")
def message_says_startwith(context, msg):
    y = wait_element_visibility(context.browser, By.ID, "result_modal", 60)
    assert y.is_displayed() is True
    x = wait_element_visibility(context.browser, By.ID, "result_modal_text", 60)
    x_string = x.get_attribute('innerText')
    if not x_string.startswith(msg):
        print("x_string: %s \ndid not match: %s" % (x_string, msg))
    assert x_string.startswith(msg) is True


@then("There is a modal that contains {msg} in it's message")
def message_contains(context, msg):
    y = context.browser.find_element_by_id("result_modal")
    assert y.is_displayed() is True
    x = context.browser.find_element_by_id("result_modal_text")
    x_string = x.get_attribute('innerText')
    assert msg in x_string


@then("The element {id} does not exist")
def does_not_exist(context, id):
    try:
        context.browser.find_element_by_id(id)
        assert False
    except Exception as e:
        logging.warning("Exception at: {}".format(e))
        assert True


@then("The tag {tag} does exist")
def tag_does_exist(context, tag):
    try:
        wait_element_presence(context.browser, By.TAG_NAME, tag, 3)
        context.browser.find_element_by_tag_name(tag)
        assert True
    except Exception as e:
        logging.warning("Exception at: {}".format(e))
        assert False


@then("The element {id} does exist")
def does_exist(context, id):
    try:
        wait_element_presence(context.browser, By.ID, id, 3)
        context.browser.find_element_by_id(id)
        assert True
    except Exception as e:
        logging.warning("Exception at: {}".format(e))
        assert False


@then("The name element {name} does exist")
def name_does_exist(context, name):
    try:
        wait_element_presence(context.browser, By.NAME, name, 3)
        context.browser.find_element_by_name(name)
        assert True
    except Exception as e:
        logging.warning("Exception at: {}".format(e))
        assert False


@then("The css {css_selector} element does exist")
def css_does_exist(context, css_selector):
    try:
        wait_element_presence(context.browser, By.CSS_SELECTOR, css_selector, 3)
        context.browser.find_element_by_css_selector(css_selector)
        assert True
    except Exception as e:
        logging.warning("Exception at: {}".format(e))
        assert False


@then("The class {class_name} does exist")
def class_does_exist(context, class_name):
    try:
        wait_element_presence(context.browser, By.CLASS_NAME, class_name, 3)
        context.browser.find_element_by_class_name(class_name)
        assert True
    except Exception as e:
        logging.warning("Exception at: {}".format(e))
        assert False


@then("I refresh the page")
def refresh_page(context):
    context.browser.refresh()


@then("I wait {seconds:d} second")
@then("I wait {seconds:d} seconds")
def wait_for_modal(context, seconds):
    sleep(seconds)


@when("{client_validity} {role} credentials are entered")
def enter_valid_non_admin_credentials(context, client_validity, role):
    non_admin_username = None
    non_admin_password = None
    if role == "client":
        non_admin_username = context.random_client
        non_admin_password = 'Testing123!'

    elif role == "employee":
        non_admin_username = context.random_employee
        non_admin_password = 'Testing123!'

    pw = (non_admin_password if client_validity == "valid" else non_admin_password + "fail")
    context.browser.find_element_by_id("username").send_keys(non_admin_username)
    context.browser.find_element_by_id("password").send_keys(pw)


@then("I click on the {tab} tab")
def click_tab(context, tab):
    tabs = {'completed_transactions': 1, 'reports': 2, 'settings': 3, 'connections': 4}
    xpath_ = '//li[contains(concat(" ",normalize-space(@class)," ")," tab ")][%d]//a' % (tabs[tab])
    wait_element_retry_and_click(context.browser, By.XPATH, xpath_)


@then("logout button exists")
def logout_exists(context):
    try:
        context.browser.find_element_by_xpath("//li[2]/a[@class='waves-effect' and 1]")
        assert True
    except Exception as e:
        logging.warning("Exception at: {}".format(e))
        assert False
