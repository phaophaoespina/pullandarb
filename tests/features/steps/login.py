from behave import given, when
import time

from tests.features.environment import browse_to, wait_element_clickability, scroll_to_element
from selenium.webdriver.common.by import By

username = 'admin'
password = 'LrP72x7Fmz6xWda2'


@when("I log in")
def login(context):
    context.execute_steps(u"""
        given I am on the login page
         when I enter valid credentials
         and I click the submit button 
    """)


@when("I log out")
def logout(context):
    browse_to(context, "logout")
    assert context.browser.title == "Login"


@given("I am logged in")
def login(context):
    browse_to(context, "login")
    if context.browser.title == 'Login':
        context.execute_steps(u"""when I log in""")


@given("I am logged out")
def login(context):
    browse_to(context, "login")
    if context.browser.title != 'Login':
        context.execute_steps(u"""when I log out""")


@when("I enter {validity} credentials")
def enter_valid_credentials(context, validity):
    pw = (password if validity == "valid" else password + "fail")
    context.browser.find_element_by_id("username").send_keys(username)
    context.browser.find_element_by_id("password").send_keys(pw)


@when("I enter the username {username} and the password {password}")
def enter_given_credentials(context, username, password):
    context.browser.find_element_by_id("username").send_keys(username)
    context.browser.find_element_by_id("password").send_keys(password)


@when("I create a non-admin user named {user_name}")
def login(context, user_name):
    context.execute_steps(u'''
        When I enter {} into the newusername text input
        When I enter Testing123! into the newuserpassword text input
        Then I wait 1 second
        When I select a Employee item from the dropdown
        Then I wait 1 second
        When I click the newusersubmit button
        Then I wait 1 second
        Then There is a modal with a message that says User successfully created
    '''.format(user_name))


@when("I delete the {user_name} user")
def delete_user(context, user_name):
    row_xpath = '//div[@id="employees"]//td[text()="%s"]/..' % user_name
    dropdown_xpath = row_xpath + '//a[@class="dropdown-trigger"]'
    delete_xpath = row_xpath + '//a[text()="Delete User"]'
    confirm_xpath = '//button[text()="confirm"]'
    scroll_to_element(context.browser, By.XPATH, row_xpath)
    wait_element_clickability(context.browser, By.XPATH, dropdown_xpath).click()
    wait_element_clickability(context.browser, By.XPATH, delete_xpath).click()
    wait_element_clickability(context.browser, By.XPATH, confirm_xpath).click()
