from behave import then, when
from selenium.webdriver.common.by import By
from tests.features.environment import wait_element_clickability, wait_elements_presence, scroll_to_element, \
    wait_element_retry_and_click, wait_element_presence


@then("There is a user named {user_name} in the dashboard with role as {role}")
def check_user_dashboard(context, user_name, role):
    div_id = 'clients' if role == 'Client' else 'employees'
    row = context.browser.find_element_by_xpath(
        '//div[@id="%s"]//td[text()="%s"]/..//td[2][text()="%s"]' %
        (div_id, user_name, role))
    assert row is not None


@when("I select a {level} item from the dropdown")
def select_employee(context, level):
    context.browser.find_element_by_class_name('select-dropdown').click()
    context.browser.find_element_by_xpath("//span[1 and text()='{}']".format(level)).click()


@when("I click the clients tab")
def clients_tab(context):
    element = context.browser.find_element_by_xpath(
        "//div[@class='card-content']/ul[@class='tabs' and 1]/li[@class='tab' and 2]/a[1]")
    scroll_to_element(context.browser, element=element)
    element.click()


@when("I activate the client account {client_account}")
def activate_client(context, client_account):
    row_xpath = '//div[@id="clients"]//td[text()="%s"]/..' % client_account
    dropdown_xpath = row_xpath + '//a[@class="dropdown-trigger"]'
    activate_xpath = row_xpath + '//a[text()="Activate User"]'
    scroll_to_element(context.browser, By.XPATH, row_xpath)
    wait_element_clickability(context.browser, By.XPATH, dropdown_xpath).click()
    wait_element_clickability(context.browser, By.XPATH, activate_xpath).click()
