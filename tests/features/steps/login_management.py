from behave import given, when
from tests.features.environment import browse_to

username = 'admin'
password = 'LrP72x7Fmz6xWda2'


@when("I log in an employee")
def login(context):
    context.execute_steps(u"""
         given I am on the login page
         when valid employee credentials are entered
         and I click the submit button 
    """)


@given("I logged in as employee")
def login(context):
    browse_to(context, "login")
    if context.browser.title == 'Login':
        context.execute_steps(u"""when I log in an employee""")


@when("I click the reports link of the company assigned")
def visit_reports_tab_employee(context):
    reports = context.browser.find_element_by_xpath("//h6/a[1]")
    reports.click()

