from behave import given, then, when
from selenium.webdriver.common.by import By
import time

from tests.features.environment import wait_element_retry_and_click, scroll_to_element, wait_element_clickability


@then("There {operator} a university named {university_name} in the dashboard")
def check_university_dashboard(context, operator, university_name):
    exists = False
    x = context.browser.find_elements_by_class_name('university_name')
    for item in x:
        y = item.text.strip()
        # TODO fix bug where this fails if the number of companies over 100
        if y == university_name:
            exists = True
            break

    should_exist = operator == "is"
    assert exists == should_exist


@given("A randomly named university is created")
def create_random_university(context):
    context.execute_steps(u"""
    Given I am on the admin page
    When I click the AddNewUniversity element
    Then I wait 1 second
    When I enter random_university into the universityname text input
    And I click the submit button
    Then I wait 1 second
    Then There is a modal with a message that says university created successfully
    When I click the closemodal button
    Then I wait 1 second
    Then The title should be JFDI - Admin Dashboard
    Then There is a university named random_university in the dashboard
    """)


@when("I delete the {university_name} university")
def delete_university(context, university_name):
    del_css = "button.waves-light.btn[style='background-color: #B71C1C']"
    confirm_xpath = '//button[text()="confirm"]'
    wait_element_retry_and_click(context.browser, By.CSS_SELECTOR, del_css)
    wait_element_clickability(context.browser, By.XPATH, confirm_xpath)


@then("I delete the {university_name} university")
def university_teardown(context, university_name):
    """
    Alternative:
    This could also be done through the factory, but since the delete button is using the same factories im
    making use of it to delete the university created.
    """
    delete_xpath = '//table/tbody/tr/td[2]/ul/li[4]/a'
    confirm_xpath = '//button[text()="confirm"]'
    scroll_to_element(context.browser, By.XPATH, delete_xpath)
    wait_element_clickability(context.browser, By.XPATH, delete_xpath).click()
    wait_element_clickability(context.browser, By.XPATH, confirm_xpath).click()
