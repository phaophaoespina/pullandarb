Feature: User Edit Page: Assign Company
  Background:
    Given I am logged in
    Given I am on the admin page
    When I click the users-tab element
    Then The element scholar-tab does exist

  Scenario: A company for user mapping is created
    Given A randomly named company is created

  Scenario: There should be a success message upon updating user mapping for scholars
    When I click the scholar-tab element
    When I click the class dropdown-trigger element
    Then I wait 1 second
    Then The element user_company_table does exist
    When I click the editUserModalSubmit element
    Then I wait 1 second
    Then There is a modal that prompts a message that starts with User
