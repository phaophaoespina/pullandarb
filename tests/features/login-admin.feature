Feature: There is a working login page
  Background:
    Given I am logged out

  Scenario: Visit login page
    Given I am on the login page
    Then The title should be Login

  Scenario: The login page should have a login form with the correct elements
    Given I am on the login page
    Then The title should be Login
    Then The login form's 1 field is u
    And The login form's 2 field is p
    And The login form's 3 field is csrf
    And There is a LET ME IN. button on the login form
