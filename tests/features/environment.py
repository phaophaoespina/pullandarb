from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException

from functools import wraps
from behave import Given, Then, When
import behave

import uuid
import configparser
import os
import time
import logging
import re


filename = os.getenv("testconfig")
if filename is None:
    filename = "test.ini"
config = configparser.ConfigParser()
config.read(filename)
settings = config['testing']
TEST_URL = settings['scheme'] + settings['domain']


def check_selenium_ready():
    import requests
    resp = requests.get("seleniumawsurl")
    return resp.json()['value']['ready']


def before_all(context):
    desired_capabilities = webdriver.DesiredCapabilities.CHROME
    desired_capabilities['name'] = 'Testing Selenium with Behave'

    if settings['selenium.driver'] == "docker":
        context.browser = webdriver.Remote(
            desired_capabilities=desired_capabilities,
            command_executor="http://localhost:4445/wd/hub"
        )
    elif settings['selenium.driver'] == "remote":
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument("--start-maximised")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--whitelisted-ips")
        while not check_selenium_ready():
            time.sleep(5)
        context.browser = webdriver.Remote(
            options=chrome_options,
            command_executor="seleniumawsurl"
        )

    # set random vars on global scope
    context.random_admin = str(uuid.uuid4())
    context.random_employee = str(uuid.uuid4())
    context.random_client = str(uuid.uuid4())
    context.random_company = str(uuid.uuid4())
    context.random_ethereum = str(uuid.uuid4())
    context.random_token = str(uuid.uuid4())[:8].upper()


def after_all(context):
    context.browser.quit()


def browse_to(context, target):
    context.browser.get(TEST_URL + "/" + target)


def wait_element_presence(driver, by, value, timeout=20):
    return WebDriverWait(driver, timeout, 1).until(EC.presence_of_element_located((by, value)))


def wait_element_visibility(driver, by, value, timeout=20):
    return WebDriverWait(driver, timeout).until(EC.visibility_of_element_located((by, value)))


def wait_element_clickability(driver, by, value, timeout=20):
    element = WebDriverWait(driver, timeout).until(EC.element_to_be_clickable((by, value)))
    while int(element.value_of_css_property('opacity')) < 1:
        time.sleep(0.01)
    return element


def wait_elements_presence(driver, by, value, timeout=20):
    return WebDriverWait(driver, timeout).until(EC.presence_of_all_elements_located((by, value)))


def wait_elements_visibility(driver, by, value, timeout=20):
    return WebDriverWait(driver, timeout).until(EC.visibility_of_all_elements_located((by, value)))


def wait_element_retry_and_click(browser, by, value, timeout=25, retry=5):
    for i in range(retry):
        try:
            wait_element_clickability(browser, by, value, timeout).click()
            break
        except TimeoutException:
            if i < retry - 1:
                browser.refresh()
            else:
                raise
        except NoSuchElementException as e:
            browser.refresh()
            logging.warning("Element not found for {}".format(e))
        except ElementClickInterceptedException:
            continue


def scroll_to_element(driver, by=None, value=None, element=None):
    # vertically scroll element to middle of browser screen
    if not element:
        element = wait_element_presence(driver, by, value)
    script = 'window.scrollTo(0, arguments[0].offsetTop - window.innerHeight/2)'
    driver.execute_script(script, element)


def get_wrapper(orig_decorator):
    def modified_decorator(pattern):
        def decorator(f):
            @orig_decorator(pattern)
            @wraps(f)
            def wrapper(context, **kwargs):
                for k, v in kwargs.items():
                    if type(v) is str:
                        for var in re.findall(r'random_[a-zA-Z0-9]+', v):
                            if not hasattr(context, var):
                                setattr(context, var, str(uuid.uuid4()))
                            kwargs[k] = v.replace(var, getattr(context, var))
                            print(context, var, kwargs[k])
                return f(context, **kwargs)
            return wrapper
        return decorator
    return modified_decorator


behave.given = get_wrapper(Given)
behave.when = get_wrapper(When)
behave.then = get_wrapper(Then)
