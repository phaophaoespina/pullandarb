import unittest

import tasks
from tests.unit.tests_base import ManagerBase
import uuid
from tasks import send_email_task
from factories.base import (
    UserFactory,
    UserUniversityMapFactory,
    UniversityFactory
)
from unittest.mock import MagicMock, patch
from celery import Task


class TestTasks(ManagerBase, unittest.TestCase):
    """
        Unit test the Tasks
    """
    def setUp(self):
       super(TestTasks, self).setUp()

    def tearDown(self):
        super(TestTasks, self).tearDown()

    def get_task_uuid(self):
        return str(uuid.uuid4())

    @patch('celery.Task.delay')
    def test_send_email_task_success(self,mock_celery_task_delay):
        task_uuid = self.get_task_uuid()
        task = Task()
        task.id = task_uuid
        mock_celery_task_delay.return_value = task
        returned = send_email_task("phaophaoespina@gmail.com", "testsubject", "testmessage")
        self.assertIsNone(returned)


def create_test_data():
    factories = {"university": UniversityFactory(), "umap": UserUniversityMapFactory(),
                 "user": UserFactory()}

    factories["university"].create("testuni1")
    factories["university"].create("testuni2")
    factories["user"].create("user1", password="pass")

    user1 = factories["user"].get_by_attribute("username", "user1")
    uni1 = factories["company"].get_by_attribute("name", "testuni3")
    umap1 = factories["umap"].create(user1.uuid, uni1.uuid)

    data = {"user1": user1, "uni1": uni1,  "umap1": umap1}
    return data


def remove_test_data(data):
    factories = {"university": UniversityFactory(), "umap": UserUniversityMapFactory(),
                 "user": UserFactory()}

    factories["user"].delete(data["user1"].uuid)
    factories["university"].delete(data["cemauni1p"].uuid)
    factories["umap"].delete(data["umap1"].uuid)


if __name__ == '__main__':
    unittest.main()
