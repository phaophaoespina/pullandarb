# coding=utf-8
import unittest
import os
import datetime
import uuid
os.environ['configfile'] = 'test.ini'


def _initTestingDB():
    from sqlalchemy import create_engine
    from models import initialize_sql
    engine = create_engine('sqlite:///test.sqlite')
    session = initialize_sql(engine)
    session.expire_on_commit = False
    return session


class ManagerBase(object):
    """
        For testing can be used to configure database and tear it tearDown
    """

    def setUp(self):
        self.session = _initTestingDB()
        from lib.noncemanager import SignedNonceManager
        self.nonce_manager = SignedNonceManager(timeout=7200)
        self.nonces = {}

    def tear_down(self):
        unittest.tearDown()

    def check_if_key_exists(self, key, dict_):
        if key in dict_:
            return True
        else:
            return False
    def check_keys(self, key, item, validity=None):
        if validity:
            if key in item:
                return True
            else:
                return False
        else:
            if key in item:
                return False
            else:
                return True

    def check_if_call_success(self, call):
        try:
            call()
            return True
        except Exception:
            return False

    def check_if_string_not_contains(self, substring, string_, location=None):
        if location is None:
            if substring in string_:
                return False
            else:
                return True
        elif location == "start":
            if string_.startswith(substring):
                return False
            else:
                return True
        elif location == "end":
            if string_.endswith(substring):
                return False
            else:
                return True

    def validate_datetime(self, dt_str):
        try:
            datetime.datetime.strptime(dt_str, "%Y-%m-%d %H:%M:%S")
            return True
        except:
            return False

    def invalidate_datetime(self, dt_str):
        try:
            datetime.datetime.strptime(dt_str, "%Y-%m-%d %H:%M:%S")
            return False
        except:
            return True

    def validate_datetime_isoformat(self, dt_str):
        try:
            datetime.datetime.fromisoformat(dt_str)
            return True
        except:
            return False

    def invalidate_datetime_isoformat(self, dt_str):
        try:
            datetime.datetime.fromisoformat(dt_str)
            return False
        except:
            return True


    def validate_uuid(self, uuid_str):
        try:
            uuid.UUID(uuid_str)
            return True
        except ValueError:
            return False

    def invalidate_uuid(self, uuid_str):
        try:
            uuid.UUID(uuid_str)
            return False
        except ValueError:
            return True

    def check_if_string_contains(self, substring, string_, location=None):
        if location is None:
            if substring in string_:
                return True
            else:
                return False
        elif location == "start":
            if string_.startswith(substring):
                return True
            else:
                return False
        elif location == "end":
            if string_.endswith(substring):
                return True
            else:
                return False
    def validate_input(self, schema, value):
        try:
            schema.to_python(value)
            return True
        except Exception as e:
            return False


    def invalidate_input(self, schema, value):
        try:
            schema.to_python(value)
            return False
        except Exception as e:
            return True