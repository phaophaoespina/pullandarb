import unittest
import datetime

import lib.axieinfinity
from tests.unit.tests_base import ManagerBase
from lib.helpers import settings
import xlsxwriter
from unittest.mock import MagicMock, patch, Mock
from lib.axieinfinity import Axie


class TestAxieInifnity(ManagerBase, unittest.TestCase):
    """
        Unit test the Coinbase
    """

    def setUp(self):
        self.axie = Axie()

    def test_get_latest_slp_price_not_none(self):
        options = self.axie.get_slp_latest_price()
        self.assertIsNotNone(options)

    def test_get_latest_slp_price_has_right_keys(self):
        valid_keys = ['php', 'usd']
        options = self.axie.get_slp_latest_price()
        for key in valid_keys:
            if self.check_keys(key, options, True):
                assert True

    @patch('lib.axieinfinity.Axie.load_addresses')
    def test_load_addresses_success(self, mock_load_addresses):
        addresses_samples = ['ronin:d5e34c80af32029706a5167d50c36900cc93cd34',
                             'ronin:3eb7a0dbfc9816cc62eaaf3059571f4116af82c1']
        self.axie.load_addresses(addresses_samples)
        self.assertTrue(mock_load_addresses.called)

    @patch('lib.axieinfinity.Axie.get_address_data')
    def test_get_addresses_data_success(self, mock_get_address_data):
        self.axie.get_address_data()
        self.assertTrue(mock_get_address_data.called)

    def test_get_latest_address_data_fail(self):
        addresses_samples = ['ronin:d5e34c80af32029706a5167d50c36900cc93cd34',
                             'ronin:3eb7a0dbfc9816cc62eaaf3059571f4116af82c1']
        self.axie.load_addresses(addresses_samples)
        self.assertRaises(TypeError, self.axie.get_latest_addresses_data)

    def test_profit_loss_calculation(self):
        all_profit = self.axie.profit_loss_calculation(20000, 5000, 5000)
        self.assertEqual(all_profit, -10000.0)


if __name__ == '__main__':
    unittest.main()
