import unittest
import datetime
import uuid
from botocore.exceptions import ClientError
from tests.unit.tests_base import ManagerBase
from lib.helpers import settings
import random
import string
import lib.helpers as helpers
import xlsxwriter
from lib.email import SES, SMTP
import lib.validation as validation

from unittest.mock import MagicMock, patch, Mock
from formencode.api import Invalid
from botocore.stub import Stubber


class TestLibraries(ManagerBase, unittest.TestCase):
    """
        Unit test the Helpers
    """

    def setUp(self):
        super(TestLibraries, self).setUp()
        self.ses_client = SES()
        self.smtp_client = SMTP()
        self.valid_uuid = validation.ValidUUID()
        self.date8601 = validation.Date8601()
        self.in_list = validation.InList()
        self.report_type = validation.ReportType()
        self.response_mock = Mock()


    def __get_address_data(self):
        return {
           "0xE72F79190BC8f92067C6A62008656c6a9077F6AA":{
              "address":"0xe72f79190bc8f92067c6a62008656c6a9077f6aa",
              "ETH":{
                 "balance":29.614896891520512,
                 "price":{
                    "rate":2169.4048369324905,
                    "diff":0.72,
                    "diff7d":1.77,
                    "ts":1618281422,
                    "marketCapUsd":250449485024.67325,
                    "availableSupply":115446172.499,
                    "volume24h":22050986853.192776,
                    "diff30d":14.83107620596951,
                    "volDiff1":11.01858761898049,
                    "volDiff7":-4.612565016654827,
                    "volDiff30":-10.35909712935026
                 }
              },
              "countTxs":155
           }
        }

    def test_valid_uuid(self):
        test_uuid = str(uuid.uuid4())
        valid_uuid = self.valid_uuid._convert_to_python(test_uuid, "teststate")
        self.assertTrue(self.validate_uuid(valid_uuid))

    def test_invalid_uuid(self):
        test_uuid = "1234567"
        self.assertRaises(Invalid, self.valid_uuid._convert_to_python, str(test_uuid), "teststate")

    def test_dat8601_valid(self):
        test = datetime.datetime.now().isoformat()
        datetimeisoformat = self.date8601._convert_to_python(str(test), "teststate")
        self.assertTrue(self.validate_datetime_isoformat(datetimeisoformat))

    def test_dat8601_invalid(self):
        test ="12/12/12"
        self.assertRaises(Invalid, self.date8601._convert_to_python, str(test), "teststate")

    def test_date_format_valid(self):
        dt_today = datetime.datetime.today()
        date_format = helpers.date_format(dt_today)
        self.assertTrue(self.validate_datetime(date_format))

    @patch('lib.helpers.date_format')
    def test_date_format_invalid(self, mock_date_format):
        dt_today = "12/12/12"
        helpers.date_format(dt_today)
        self.assertTrue(mock_date_format.called)
        call_args = mock_date_format.call_args
        self.assertTrue(self.invalidate_datetime(call_args))

    @patch('lib.email.SES.send_email')
    def test_send_ses_email_success(self, mock_boto_client):
        self.ses_client.send_email("test@gmail.com", "testfrom@gmail.com", "testbody")
        self.assertTrue(mock_boto_client.called)
        boto_response = mock_boto_client.call_args[0][0]
        self.assertTrue(boto_response)
        self.assertTrue(self.check_if_string_contains("@", boto_response))

    @patch('lib.email.SES.send_email')
    def test_send_ses_email_fail(self, mock_boto_client):
        self.ses_client.send_email("test@gmail.com", "testfrom@gmail.com", "testbody")
        self.assertTrue(mock_boto_client.called)
        boto_response = "testresponsestr"
        self.assertTrue(boto_response)
        self.assertFalse(self.check_if_string_contains("@", boto_response))

    @patch('lib.email.SES.send_email')
    def test_send_ses_email_exception(self, mock_boto_client):
        parsed_response = {'Error': {'Code': '500', 'Message': 'Error Sending Email'}}
        mock_boto_client.side_effect = ClientError(parsed_response, "SESSendEMail")
        self.assertRaises(ClientError, self.ses_client.send_email, "test@gmail.com", "testfrom@gmail.com", "testbody")

    @patch('lib.email.SMTP.send_email')
    def test_send_smtp_email_success(self, mock_smtp_client):
        self.smtp_client.send_email("pespina@tracehop.com", "pespina@tracehop.com", "testbody")
        self.assertTrue(mock_smtp_client.called)
        smtp_response = mock_smtp_client.call_args[0][0]
        self.assertTrue(smtp_response)
        self.assertTrue(self.check_if_string_contains("@", smtp_response))

    @patch('lib.email.SMTP.send_email')
    def test_send_smtp_email_fail(self, mock_boto_client):
        self.smtp_client.send_email("pespina@tracehop.com", "pespina@tracehop.com", "testbody")
        self.assertTrue(mock_boto_client.called)
        smtp_response = "testresponsestr"
        self.assertTrue(smtp_response)
        self.assertFalse(self.check_if_string_contains("@", smtp_response))

    def test_get_hash_password_is_str(self):
        length = 32
        random.seed(datetime.datetime.now())
        vc = string.ascii_letters + string.digits
        h1 = ''.join([random.choice(vc) for i in range(32)])
        hashed_password = helpers.gen_hash_password(h1, length)
        self.assertEqual(length, len(hashed_password))
        self.assertIsInstance(hashed_password, str)

    @patch('lib.helpers.gen_hash_password')
    def test_get_hash_password_invalid(self, mock_gen_hash_password):
        length = 12
        h1 = "random string"
        helpers.gen_hash_password(h1, length)
        self.assertTrue(mock_gen_hash_password.called)
        call_args = mock_gen_hash_password.call_args[0][0]
        self.assertEqual(call_args, h1)
        self.assertNotEqual(length, len(call_args))

    def test_gen_password_is_str(self):
        generated_password = helpers.generate_password()
        self.assertIsInstance(generated_password, str)


    def test_redis_encode_decode(self):
        test_data = {'test': True}
        encoded = helpers.redis_encode(test_data)
        self.assertIsInstance(encoded, bytes)
        '''
        Found error here:
        
        _pickle.UnpicklingError: A load persistent id instruction was encountered,
        but no persistent_load function was specified.
        '''

        # base64_message = 'UHl0aG9uIGlzIGZ1bg=='
        # decoded = helpers.redis_decode(base64_message)
        # self.assertIsInstance(decoded, str)




if __name__ == '__main__':
    unittest.main()
