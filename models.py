"""
Models associated with database
"""
from builtins import int

import sqlalchemy
from sqlalchemy import (
    Column,
    ForeignKey,
    Text,
    String,
    DateTime,
    Binary,
    LargeBinary
)

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, scoped_session, sessionmaker

import datetime
import uuid
import logging
from sqlalchemy import engine_from_config
from zope.sqlalchemy import register
from lib.helpers import settings

from sqlalchemy import event
from sqlalchemy import exc
import os

engine = engine_from_config(settings, 'sqlalchemy.', pool_recycle=3600, encoding='utf-8')
if settings['test'] == "true":
    engine.echo = True
Session = sessionmaker(autocommit=False)
DBSESSION = scoped_session(Session)
register(DBSESSION)
Base = declarative_base()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


@event.listens_for(engine, "connect")
def connect(dbapi_connection, connection_record):
    connection_record.info['pid'] = os.getpid()


@event.listens_for(engine, "checkout")
def checkout(dbapi_connection, connection_record, connection_proxy):
    pid = os.getpid()
    if connection_record.info['pid'] != pid:
        connection_record.connection = connection_proxy.connection = None
        raise exc.DisconnectionError(
            "Connection record belongs to pid %s, "
            "attempting to check out in pid %s" %
            (connection_record.info['pid'], pid)
        )


def initialize_sql(eng=None):
    # Initial SQL connection and create tables if needed
    if eng is None:
        eng = engine
    DBSESSION.configure(bind=eng)
    Base.metadata.bind = eng
    eng.dispose()
    if settings.get("resetdb") == 'true':
        Base.metadata.drop_all(eng)
    # Create the tables if they don't exist
    Base.metadata.create_all(eng, checkfirst=True)
    if settings.get("test") == 'true':
        session = DBSESSION()
        if settings.get("resetdb") == 'true':
            import transaction

            user = User("admin", "admin", "LrP72x7Fmz6xWda2")
            session.add(user)

            exchanges = ["axieinfinity"]
            for x in exchanges:
                exchange_to_add = Exchange(x)
                session.add(exchange_to_add)
            session.flush()
            transaction.commit()
        return session


class Encrypt(object):
    """ For models that have a field with encryption """

    def randstr(self, length):
        import random
        return ''.join(chr(random.randint(0, 255)) for i in range(length))

    def s_password(self):
        """
            Returns the encrypted value.
        """
        return self.__dict__[self.encrypt_value]

    def _set_password(self, pwd, maxtime=0.5, datalength=64):
        """
            Encrypts password on the fly.
        """
        import scrypt
        self.__dict__[self.encrypt_value] = scrypt.encrypt(self.randstr(datalength), pwd,
                                                           maxtime=maxtime)

    def validate(self, value_check):
        """
            Check the value against existing credentials.
            this method returns a boolean.

            @param value: the value that was provided by the user to
            try and authenticate. This is the clear text version that we will
            need to match against the encrypted we store.
        """
        import scrypt
        try:
            scrypt.decrypt(self.__dict__[self.encrypt_value], str(value_check), maxtime=10.0)
            return True
        except Exception as e:
            logging.warning("Exception found at: {}".format(e))
            return False


class Uuid(object):

    def str_uuid(self):
        return str(uuid.uuid4())

    uuid = Column(String(36), primary_key=True, default=str_uuid, unique=True, nullable=False)
    date_created = Column(DateTime)
    date_updated = Column(DateTime)


class User(Encrypt, Uuid, Base):
    __tablename__ = 'users'
    username = Column(String(255), unique=False)
    _password = Column('password', Binary(256))
    encrypt_value = "_password"
    authority = Column(String(20))
    status = Column(String(20))
    secret_key = Column(String(32))
    auth_status = Column(String(20))
    app_login = Column(String(20))
    app_password = Column(String(20))
    ronin_address = Column(String(256))
    scholar_share = Column(String(256))

    qr_data = Column(LargeBinary, nullable=True)
    team_aquisition_cost = Column(String(256))
    team_current_valuation = Column(String(256))

    def __init__(self, username, authority, password=None, status=None, secret_key=None, auth_status=None,
                 app_login=None, app_password=None, ronin_address=None, scholar_share=None,
                 team_aquisition_cost=None, team_current_valuation=None, qr_data=None):
        self.username = username
        self.authority = authority
        self.status = status
        self.secret_key = secret_key
        self.auth_status = auth_status
        self.app_login = app_login
        self.app_password = app_password
        self.ronin_address = ronin_address
        self.qr_data = qr_data
        self.scholar_share = scholar_share
        self.team_aquisition_cost = team_aquisition_cost
        self.team_current_valuation = team_current_valuation

        self.date_updated = self.date_created = datetime.datetime.now()
        if password is not None:
            self.set_password(password)

    def set_password(self, password):
        self.password = self._set_password(password)

    def validate(self, value_check):
        if value_check is not None:
            return super(User, self).validate(value_check)
        return False


class AxieDataResponse(Encrypt, Uuid, Base):
    __tablename__ = 'axiedataresponse'

    user_uuid = Column(String(36), ForeignKey('users.uuid'))
    ign = Column(String(255))
    updated_on = Column(String(255))
    total_slp = Column(String(20))
    in_game_slp = Column(String(20))
    ronin_slp = Column(String(32))
    win_rate = Column(String(20))
    mmr = Column(String(20))
    rank = Column(String(20))
    total_matches = Column(String(256))
    last_claim_amount = Column(String(256))
    last_claim_timestamp = Column(String(256))
    scholar_payout = Column(String(256))

    def __init__(self, user_uuid, ign=None, updated_on=None, total_slp=None, in_game_slp=None, ronin_slp=None,
                 win_rate=None, mmr=None, rank=None, total_matches=None, last_claim_amount=None,
                 last_claim_timestamp=None, scholar_payout=None):
        self.ign = ign
        self.updated_on = updated_on
        self.total_slp = total_slp
        self.in_game_slp = in_game_slp
        self.ronin_slp = ronin_slp
        self.win_rate = win_rate
        self.mmr = mmr
        self.rank = rank
        self.scholar_payout = scholar_payout
        self.total_matches = total_matches
        self.last_claim_amount = last_claim_amount
        self.last_claim_timestamp = last_claim_timestamp
        self.date_updated = self.date_created = datetime.datetime.now()
        self.user_uuid = user_uuid


class Exchange(Uuid, Base):
    __tablename__ = 'exchanges'
    name = Column(String(255))

    def __init__(self, name):
        self.date_updated = self.date_created = datetime.datetime.now()
        self.name = name


class University(Uuid, Base):
    __tablename__ = 'universities'
    name = Column(String(255))
    created_by = Column(String(255))

    def __init__(self, name, created_by):
        self.date_updated = self.date_created = datetime.datetime.now()
        self.name = name
        self.created_by = created_by


class UserUniversityMap(Uuid, Base):
    __tablename__ = 'useruniversitymap'
    user_uuid = Column(String(36), ForeignKey('users.uuid'))
    university_uuid = Column(String(36), ForeignKey('universities.uuid'))

    def __init__(self, user_uuid, university_uuid):
        self.date_updated = self.date_created = datetime.datetime.now()
        self.user_uuid = user_uuid
        self.university_uuid = university_uuid


class AdminUserMap(Uuid, Base):
    __tablename__ = 'adminusermap'
    admin_uuid = Column(String(36), ForeignKey('users.uuid'))
    user_uuid = Column(String(36), ForeignKey('users.uuid'))

    def __init__(self, admin_uuid, user_uuid):
        self.date_updated = self.date_created = datetime.datetime.now()
        self.admin_uuid = admin_uuid
        self.user_uuid = user_uuid


class UserSettings(Uuid, Base):
    __tablename__ = 'user_settings'
    created_by = Column(String(36), ForeignKey('users.uuid'))
    university_uuid = Column(String(36), ForeignKey('universities.uuid'))
    settings = Column(Text)

    def __init__(self, created_by, university_uuid, settings):
        self.date_updated = self.date_created = datetime.datetime.now()
        self.created_by = created_by
        self.university_uuid = university_uuid
        self.settings = settings
